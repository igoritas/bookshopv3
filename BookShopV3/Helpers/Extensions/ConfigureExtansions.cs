﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace BookShopV3.Helpers.Extensions
{
	static public class ConfigureExtansions
	{
		public static void AddCors(this IServiceCollection services, string namePolice)
		{
			services.AddCors(opt =>
			{
				opt.AddPolicy(namePolice, builder =>
				{
					builder.WithOrigins("http://localhost:4200").AllowAnyHeader().AllowCredentials().AllowAnyMethod();
				});
			});
		}
		public static void AddSwwager(this IServiceCollection services)
		{
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
			});
		}
		public static void UseSwaggerApp(this IApplicationBuilder app)
		{
			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
				c.RoutePrefix = string.Empty;
			});
		}

	}
}
