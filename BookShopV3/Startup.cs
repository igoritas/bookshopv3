﻿using BookShopV3.BusisnesLogicLayer.Helpers.Extensions;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using BookShopV3.Helpers.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace BookShopV3
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		[Obsolete]
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();
			services.AddSwwager();
			services.AddCors("BookShop");
			services.ServiceInitialiaz(Configuration);



			JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
			services
				.AddAuthentication(options =>
				{
					options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
					options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
					options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
				})
				.AddJwtBearer(cfg =>
				{
					cfg.RequireHttpsMetadata = false;
					cfg.SaveToken = true;
					cfg.TokenValidationParameters = new TokenValidationParameters
					{
						ValidIssuer = Configuration.GetSection("JwtOption")["JwtIssuer"],
						ValidAudience = Configuration.GetSection("JwtOption")["JwtIssuer"],
						IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("JwtOption")["JwtKey"])),
						ClockSkew = TimeSpan.Zero
					};
				});


			services.AddMvcCore().AddApiExplorer();
		}
		[Obsolete]
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerService loggerService)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			if (!env.IsDevelopment())
			{
				app.UseHsts();
			}
			
			app.UseHttpsRedirection();
			app.UseStaticFiles();
			
			app.UseCors("BookShop");

			app.ExceptionHandler(loggerService);
			app.UseRouting();
			app.UseAuthentication();
			app.UseAuthorization();
			app.UseSwaggerApp();
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
