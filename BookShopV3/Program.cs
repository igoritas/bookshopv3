﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;

namespace BookShopV3
{
	public class Program
	{
		public static async Task Main(string[] args)
		{
			 CreateHostBuilder(args).Build().Run();
		}

		public static IWebHostBuilder CreateHostBuilder(string[] args) =>
		   WebHost.CreateDefaultBuilder()
			.UseStartup<Startup>();		
			
	}
}
