﻿namespace BookShopV3.Constaint
{
	partial class PresentationLayerConstant
	{
		public partial class AccountEmailConstant
		{
			public partial class UrlConstant
			{
				public const string MethodConfirm = "ConfirmEmail";
				public const string ControllerConfirm = "Account";
			}
		}
	}
}
