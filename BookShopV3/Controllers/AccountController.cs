﻿using BookShopV3.BusisnesLogicLayer.Helpers;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using BookShopV3.BusisnesLogicLayer.Models;
using BookShopV3.BusisnesLogicLayer.Models.Token;
using BookShopV3.Constaint;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookShopV3.Controllers
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class AccountController : ControllerBase
	{
		private readonly IUserService _userService;
		private readonly JWTHelpers _jWTHelpers;

		public AccountController(IUserService userService, JWTHelpers jWTHelpers)
		{
			_userService = userService;
			_jWTHelpers = jWTHelpers;
		}
		private void SetCookieTokenResponse(string accessToken, string refreshToken)
		{
			Response.Cookies.Append("asseccToken", accessToken);
			Response.Cookies.Append("refreshToken", refreshToken);
		}
		[HttpPost]
		public async Task<IActionResult> Login([FromBody] LoginViewModel user)
		{
			var userModel = await _userService.LoginAsync(user.Login, user.Password);
			if (userModel.Errors.Count != 0)
			{
				return Unauthorized(userModel.Errors);
			}
			var jwtTokenModel = _jWTHelpers.GenerateAccessAndRefreshToken(user.Login, userModel.Id);
			SetCookieTokenResponse(jwtTokenModel.AccessToken, jwtTokenModel.RefreshToken);
			return Ok(userModel);
		}
		[HttpPost]
		public IActionResult Refresh(JwtTokenModel refreshTokenModel)
		{
			var jwtTokenModel = new JwtTokenModel();
			var principal = _jWTHelpers.GetPrincipalFromExpiredToken(refreshTokenModel.AccessToken);
			jwtTokenModel.AccessToken = _jWTHelpers.GenerateJwtToken(principal.Claims);
			jwtTokenModel.RefreshToken = _jWTHelpers.GenerateRefreshToken();
			return new ObjectResult(jwtTokenModel);
		}

		[HttpPost]
		public async Task LogOff()
		{
			await _userService.LogOutAsync();
		}
		[HttpPost]
		public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
		{
			var userModel = await _userService.CreateAsync(model);
			if (userModel.Errors.Count != 0)
			{
				return Ok(userModel.Errors);
			}
			var emailConfirmToken = await _userService.GenerateEmailConfirmationTokenAsync(userModel);
			var callbackUrl = Url.Action(
				   PresentationLayerConstant.AccountEmailConstant.UrlConstant.MethodConfirm,
				   PresentationLayerConstant.AccountEmailConstant.UrlConstant.ControllerConfirm,
				   new
				   {
					   email = userModel.Email,
					   code = emailConfirmToken
				   },
				   protocol: HttpContext.Request.Scheme);
			await _userService.SendEmailComfirmTokenAsync(callbackUrl, model.Email);
			await _userService.SignInAsync(userModel);
			var jwtTokenModel = _jWTHelpers.GenerateAccessAndRefreshToken(model.Login, userModel.Id);
			SetCookieTokenResponse(jwtTokenModel.AccessToken, jwtTokenModel.RefreshToken);
			return Ok(userModel);
		}
		[HttpGet("{email}/{code}")]
		public async Task<ActionResult> ConfirmEmail(string email, string code)
		{
			code = code.Replace("%2F", "/");
			var result = await _userService.ConfirmEmailAsync(email, code);
			return Ok(result);
		}


		[HttpGet("{email}")]
		public async Task<IActionResult> ForgotPassword(string email)
		{
			var result = await _userService.ForgotPasswordAsync(email);
			return Ok(result);
		}
	}
}