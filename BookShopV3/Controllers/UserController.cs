﻿using errorConstant = BookShopV3.BusinessLogicLayer.Constant.Error.BusinessLogicLayerСonstant.ErrorСonstant;
using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookShopV3.Controllers
{

	[Authorize]
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly IUserService _userService;

		public UserController(IUserService userService)
		{
			_userService = userService;
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetById(string id)
		{
			var errors = new BaseModel();
			var result = await _userService.GetByIdAsync(id);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorUserСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(result);
		}

		[HttpPost]
		public async Task<IActionResult> GetAll([FromBody]FilterRequestModel filters)
		{
			var errors = new BaseModel();
			var result = await _userService.GetFilteringUserAsync(filters);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorUserСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(result);
		}

		[HttpPut]
		public async Task<IActionResult> Update([FromBody] EditUserModel user)
		{
			var userClaims = HttpContext.User.Identities.First().Claims.ToList();
			var userID = userClaims?.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase))?.Value;
			var result = await _userService.UpdateAsync(user, userID);
			if (result.Errors.Count != 0)
			{
				return BadRequest(result.Errors);
			}
			return Ok(result);

		}
		[HttpDelete("{Id}")]
		public async Task<IActionResult> Delete(string Id)
		{
			var errors = new BaseModel();
			var result = await _userService.DeleteAsync(Id);
			if (result.Errors.Count != 0)
			{
				errors.Errors.AddRange(result.Errors);
			}
			return Ok(errors);

		}

	}
}