﻿using errorConstant = BookShopV3.BusinessLogicLayer.Constant.Error.BusinessLogicLayerСonstant.ErrorСonstant;
using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using BookShopV3.BusisnesLogicLayer.Models.FilterModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookShopV3.Controllers
{
	[Authorize]
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class PrintingEditionController : ControllerBase
	{
		private readonly IPrintingEditionService _bookService;
		public PrintingEditionController(IPrintingEditionService bookService)
		{
			this._bookService = bookService;
		}
		[HttpGet("{id}")]
		public async Task<IActionResult> GetById(string id)
		{
			var errors = new BaseModel();
			var result = await _bookService.GetByIdAsync(id);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorPrintingEditionСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(result);
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IActionResult> GetAll([FromBody]FilterPrintingEditionModel fileterModel)
		{
			var errors = new BaseModel();
			var result = await _bookService.GetAllAsync(fileterModel);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorPrintingEditionСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(result);

		}
		[HttpPost]
		public async Task<IActionResult> Add([FromBody] PrintingEditionWithAuthorModel book)
		{
			var errors = new BaseModel();
			var result = await _bookService.AddAsync(book);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorPrintingEditionСonstant.Create);
			}
			return Ok(errors);
		}
		[HttpPut]
		public async Task<IActionResult> Update([FromBody] PrintingEditionWithAuthorModel book)
		{
			var errors = new BaseModel();
			var result = await _bookService.UpdateAsync(book);
			if (result == 0)
			{
				errors.Errors.Add(errorConstant.ErrorPrintingEditionСonstant.Update);
			}
			return Ok(errors);
		}
		[HttpDelete("{Id}")]
		public async Task<IActionResult> Delete(string Id)
		{
			var errors = new BaseModel();
			var result = await _bookService.DeleteAsync(Id);
			if (result == 0)
			{
				errors.Errors.Add(errorConstant.ErrorPrintingEditionСonstant.Delete);
			}
			return Ok(errors);
		}

	}
}