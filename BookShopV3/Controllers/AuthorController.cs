﻿using errorConstant = BookShopV3.BusinessLogicLayer.Constant.Error.BusinessLogicLayerСonstant.ErrorСonstant;
using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.Controllers
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	[Authorize]
	public class AuthorController : ControllerBase
	{
		private readonly IAuthorService _authorService;
		public AuthorController(IAuthorService authorService)
		{
			_authorService = authorService;
		}

		[HttpGet]
		public async Task<ActionResult<IEnumerable<AuthorWithPrintingEditionModel>>> GetAll()
		{
			var errors = new BaseModel();
			var result = await _authorService.GetAllAsync();
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorAuthorСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(result);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetById(string id)
		{
			var errors = new BaseModel();
			var result = await _authorService.GetByIdAsync(id);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorAuthorСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(result);

		}

		[HttpPost]
		public async Task<IActionResult> Add([FromBody] AuthorWithPrintingEditionModel author)
		{
			var errors = new BaseModel();
			var result = await _authorService.CreateAsync(author);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorAuthorСonstant.Create);
			}
			return Ok(errors);

		}

		[HttpPut]
		public async Task<IActionResult> Update([FromBody] AuthorWithPrintingEditionModel author)
		{
			var errors = new BaseModel();
			var resultUpdate = await _authorService.UpdateAsync(author);
			if (resultUpdate == 0)
			{
				errors.Errors.Add(errorConstant.ErrorAuthorСonstant.Update);
			}
			return Ok(errors);

		}

		[HttpDelete("{Id}")]
		public async Task<IActionResult> Delete(string Id)
		{
			var errors = new BaseModel();
			var resultDelete = await _authorService.DeleteAsync(Id);
			if (resultDelete == 0)
			{
				errors.Errors.Add(errorConstant.ErrorAuthorСonstant.Delete);
			}
			return Ok(errors);
		}

		[HttpPost]
		public async Task<IActionResult> GetFilteringData([FromBody]FilterRequestModel index)
		{
			var errors = new BaseModel();
			var resultFileting = await _authorService.GetAllAsync(index);
			if (resultFileting is null)
			{
				errors.Errors.Add(errorConstant.ErrorAuthorСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(resultFileting);

		}

	}
}
