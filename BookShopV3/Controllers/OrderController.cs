﻿using BookShopV3.BusisnesLogicLayer.Common;
using errorConstant = BookShopV3.BusinessLogicLayer.Constant.Error.BusinessLogicLayerСonstant.ErrorСonstant;
using BookShopV3.BusisnesLogicLayer.Helpers;
using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BookShopV3.BusinessLogicLayer.Helpers.Interfaces;

namespace BookShopV3.Controllers
{
	[Authorize]
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class OrderController : ControllerBase
	{
		private readonly IOrderService _orderService;
		private readonly IOptions<ConvertCurrencyOption> _convertCurrencyOptions;
		private readonly IHttpHelper _httpHelper;

		public OrderController(IOrderService orderService, IOptions<ConvertCurrencyOption> options, IHttpHelper httpHelper)
		{
			_orderService = orderService;
			_convertCurrencyOptions = options;
			_httpHelper = httpHelper;
		}
		[HttpGet("{id}")]
		public async Task<IActionResult> GetById(string id)
		{
			var errors = new BaseModel();
			var result = await _orderService.GetByIdAsync(id);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorOrderСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(result);
		}

		[HttpPost]
		public async Task<IActionResult> Add([FromBody] OrderModel order)
		{
			var errors = new BaseModel();
			var userClaims = HttpContext.User.Identities.First().Claims.ToList();
			var userID = userClaims?.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase))?.Value;
			order.User.Id = userID;
			var result = await _orderService.CreateAsync(order);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorOrderСonstant.Create);
			}
			return Ok(errors);
		}

		[HttpPut]
		public async Task<IActionResult> Update([FromBody] OrderModel order)
		{
			var errors = new BaseModel();
			var result = await _orderService.UpdateAsync(order);
			if (result.Errors.Count != 0)
			{
				errors.Errors.Add(errorConstant.ErrorOrderСonstant.Update);
			}
			return Ok(errors);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(string id)
		{
			var errors = new BaseModel();
			var result = await _orderService.DeleteAsync(id);
			if (result.Errors.Count != 0)
			{
				errors.Errors.Add(errorConstant.ErrorOrderСonstant.Delete);
			}
			return Ok(errors);
		}
		[AllowAnonymous]
		[HttpGet("{baseValut}/{ratesValut}")]
		public async Task<IActionResult> ConvertCurrency(Currency baseValut, Currency ratesValut)
		{

			var url = _convertCurrencyOptions.Value.Url;
			var json = JObject.Parse(await _httpHelper.GetHttpContent(@$"{url}?base={baseValut.ToString()}&symbols={ratesValut.ToString()}"));
			var resultCoefficient = json["rates"][ratesValut.ToString()];
			return Ok(new { result = (double)resultCoefficient });
		}

		[HttpPost]
		public async Task<IActionResult> GetAll([FromBody]FilterRequestModel filters)
		{
			var errors = new BaseModel();
			var result = await _orderService.GetAllAsync(filters);
			if (result is null)
			{
				errors.Errors.Add(errorConstant.ErrorOrderСonstant.Filtring);
				return Ok(errors);
			}
			return Ok(result);

		}
	}
}
