﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.EFRepository;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookShopV3.DataAccessLayer.Initializer
{
	public static class DataAccessInitializeExtensions
	{
		public static void RepositoryInitialiaz(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddDbContext<ApplicationContext>(options =>
			options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));

			services.AddIdentity<ApplicationUser, IdentityRole>()
			   .AddEntityFrameworkStores<ApplicationContext>()
				 .AddDefaultTokenProviders();
			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = false;
				options.Password.RequireLowercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;
				options.Password.RequiredLength = 6;
				options.Password.RequiredUniqueChars = 1;
				options.User.RequireUniqueEmail = true;
			});

			services.AddTransient<IAuthorRepository, AuthorRepository>();
			services.AddTransient<IAuthorInPrintingEditionRepository, AuthorInPrintingEditionRepository>();

			services.AddTransient<IPrintingEditionRepository, PrintingEditionRepository>();

			services.AddTransient<IOrderRepository, OrderRepository>();
			services.AddTransient<IOrderItemRepository, OrderItemRepository>();

			services.AddTransient<IUserRepository, UserRepository>();

			_ = services.BuildServiceProvider().InitializeDbAsync();
		}
	}
}
