﻿using BookShopV3.DataAccessLayer.Constaint;
using BookShopV3.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Initializer
{
	public static class RoleInitializer
	{
	
		public static async Task InitializeDbAsync(this IServiceProvider service)
		{
			var userManager = (UserManager<ApplicationUser>)service.GetService(typeof(UserManager<ApplicationUser>));
			var roleManager = (RoleManager<IdentityRole>)service.GetService(typeof(RoleManager<IdentityRole>));
			if (await roleManager.FindByNameAsync(RoleType.Admin.ToString()) == null)
			{
				await roleManager.CreateAsync(new IdentityRole(RoleType.Admin.ToString()));
			}
			if (await roleManager.FindByNameAsync(RoleType.User.ToString()) == null)
			{
				await roleManager.CreateAsync(new IdentityRole(RoleType.User.ToString()));
			}
			if (await userManager.FindByNameAsync(DataAccessConstant.AdminEmail) != null)
			{
				return;
			}
			var userName = DataAccessConstant.AdminEmail;
			var admin = new ApplicationUser
			{
				Email = DataAccessConstant.AdminEmail,
				UserName = userName,
				EmailConfirmed = true
			};
			var result = await userManager.CreateAsync(admin, DataAccessConstant.AdminPassword);
			if (result.Succeeded)
			{
				await userManager.AddToRoleAsync(admin, RoleType.Admin.ToString());
			}
		}
	}
}
