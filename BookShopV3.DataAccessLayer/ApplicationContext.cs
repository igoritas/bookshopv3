﻿using BookShopV3.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookShopV3.DataAccessLayer
{
	public class ApplicationContext : IdentityDbContext<ApplicationUser>
	{
		public DbSet<Author> Authors { get; set; }
		public DbSet<PrintingEdition> PrintingEditions { get; set; }
		public DbSet<ApplicationUser> ApplicationUsers { get; set; }
		public DbSet<AuthorInPrintingEdition> AuthorInPrintingEditions { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<OrderItem> OrderItems { get; set; }
		public DbSet<Payment> Payments { get; set; }

		public ApplicationContext()
		{
			Database.EnsureCreated();
		}

		public ApplicationContext(DbContextOptions<ApplicationContext> options)
			: base(options)
		{
		}
		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder
			.Entity<Order>()
			.HasOne(u => u.Payment)
			.WithOne(p => p.Order)
			.HasForeignKey<Order>(p => p.PaymentId);
		}
	}
}
