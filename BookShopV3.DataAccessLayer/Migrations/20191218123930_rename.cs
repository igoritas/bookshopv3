﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookShopV3.DataAccessLayer.Migrations
{
    public partial class rename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthorInPrintingEditions_PrintingEditions_BookId",
                table: "AuthorInPrintingEditions");

            migrationBuilder.DropIndex(
                name: "IX_AuthorInPrintingEditions_BookId",
                table: "AuthorInPrintingEditions");

            migrationBuilder.DropColumn(
                name: "BookId",
                table: "AuthorInPrintingEditions");

            migrationBuilder.AddColumn<Guid>(
                name: "PrintingEditionId",
                table: "AuthorInPrintingEditions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AuthorInPrintingEditions_PrintingEditionId",
                table: "AuthorInPrintingEditions",
                column: "PrintingEditionId");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorInPrintingEditions_PrintingEditions_PrintingEditionId",
                table: "AuthorInPrintingEditions",
                column: "PrintingEditionId",
                principalTable: "PrintingEditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthorInPrintingEditions_PrintingEditions_PrintingEditionId",
                table: "AuthorInPrintingEditions");

            migrationBuilder.DropIndex(
                name: "IX_AuthorInPrintingEditions_PrintingEditionId",
                table: "AuthorInPrintingEditions");

            migrationBuilder.DropColumn(
                name: "PrintingEditionId",
                table: "AuthorInPrintingEditions");

            migrationBuilder.AddColumn<Guid>(
                name: "BookId",
                table: "AuthorInPrintingEditions",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AuthorInPrintingEditions_BookId",
                table: "AuthorInPrintingEditions",
                column: "BookId");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorInPrintingEditions_PrintingEditions_BookId",
                table: "AuthorInPrintingEditions",
                column: "BookId",
                principalTable: "PrintingEditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
