﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookShopV3.DataAccessLayer.Migrations
{
    public partial class removecolumnrenamecolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PriceInOrderItem",
                table: "OrderItems");

            migrationBuilder.DropColumn(
                name: "CodeEmail",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<decimal>(
                name: "SummInOrderItem",
                table: "OrderItems",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SummInOrderItem",
                table: "OrderItems");

            migrationBuilder.AddColumn<decimal>(
                name: "PriceInOrderItem",
                table: "OrderItems",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "CodeEmail",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
