﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookShopV3.DataAccessLayer.Migrations
{
    public partial class renamecolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SummInOrderItem",
                table: "OrderItems");

            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "OrderItems",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "OrderItems");

            migrationBuilder.AddColumn<decimal>(
                name: "SummInOrderItem",
                table: "OrderItems",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
