﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.DapperRepository
{
	public class OrderRepository : BaseDapperRepository<Order>, IOrderRepository
	{
		public OrderRepository(string connectionString) : base(connectionString) { }

		public async Task<FilterResponseDataModel<Order>> GetFilteredAsync(FilterRequestDataModel filterDataModel)
		{
			var selectString = new StringBuilder();
			selectString.Append($@"SELECT *
							FROM (
							SELECT *
							FROM OrderItems AS oi
							) AS t
							INNER JOIN (
								SELECT o.Id as IdOrderInItems,o.ApplicationUserId,o.Currency,o.[Date] ,o.IsPaid, o.IsRemoved, o.PaymentId, o.Summ,u.Id as IdUserInOrder, u.Email , u.UserName
								FROM Orders AS o
								INNER JOIN(
									SELECT u.Email, u.UserName , u.Id
									FROM AspNetUsers AS u
								) AS u ON o.ApplicationUserId = u.Id
								WHERE (o.IsRemoved = 0)");

			foreach (var item in filterDataModel.FilterCheckBoxDataModels)
			{
				selectString.Append($" AND (o.{item.FiltrationСolumn} != {item.Value}) ");
			}
			selectString.Append($" ORDER BY o.{filterDataModel.SortColumnName} {filterDataModel.SortType.ToString().ToUpper()} ");
			selectString.Append($" OFFSET {filterDataModel.PageModel.PageNumber * filterDataModel.PageModel.PageSize} ROWS FETCH NEXT {filterDataModel.PageModel.PageSize} ROWS ONLY ");
			selectString.Append($@" )AS o ON o.IdOrderInItems = t.OrderId
							INNER JOIN(
							SELECT*
							FROM PrintingEditions AS pe
							)AS pe ON pe.Id = t.BookId");
			selectString.Append($@" Select Count(*) 
								FROM Orders AS P
								WHERE  (P.IsRemoved != 1)");
			foreach (var item in filterDataModel.FilterCheckBoxDataModels)
			{
				selectString.Append($" AND([p].[{item.FiltrationСolumn}] != {item.Value}) ");
			}
			var response = new FilterResponseDataModel<Order>();
			var orders = new List<Order>();
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				var query = selectString.ToString();
				using (var multi = await dbConnection.QueryMultipleAsync(query))
				{
					var resultList = multi.Read<OrderItem, Order, ApplicationUser, PrintingEdition, OrderItem>(
						(OrderItem, Order, ApplicationUser, PrintingEdition) =>
						{
							OrderItem.Book = PrintingEdition;
							Order.ApplicationUser = ApplicationUser;
							OrderItem.Order = Order;
							return OrderItem;
						},
						splitOn: "Id, IdOrderInItems, IdUserInOrder, Id"
						);
					orders = resultList.GroupBy(p => p.OrderId).Select(x => new Order()
					{
						OrderItems = x.Select(y => new OrderItem()
						{
							Order = y.Order,
							Book = y.Book,
							Id = y.Id,
							Count = y.Count,
							IsRemoved = y.IsRemoved,
							Amount = y.Amount,
							OrderId = y.OrderId
						}).ToList(),
						Id = x.Key,
						Currency = x.Select(y => y.Order.Currency).First(),
						ApplicationUser = x.Select(y => y.Order.ApplicationUser).First(),
						ApplicationUserId = x.Select(y => y.Order.ApplicationUserId).First(),
						CreationDate = x.Select(y => y.Order.CreationDate).First(),
						PayStatus = x.Select(y => y.Order.PayStatus).First(),
						Payment = x.Select(y => y.Order.Payment).First(),
						Summ = x.Select(y => y.Order.Summ).First(),
					}).ToList();

					response.AllCount = multi.Read<int>().First();
				}
				response.Items = orders;
				return response;
			}
		}


	}
}
