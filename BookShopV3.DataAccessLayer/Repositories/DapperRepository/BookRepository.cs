﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.DapperRepository
{
	public class BookRepository : BaseDapperRepository<PrintingEdition>, IPrintingEditionRepository
	{
		public BookRepository(string connectionString) : base(connectionString)
		{ }


		public async Task<FilterResponseDataModel<PrintingEdition>> GetFilteredBookAsync(FilterRequestDataModel filterDataModel, decimal minPrice = 0, decimal maxPrice = decimal.MaxValue)
		{
			var selectString = new StringBuilder();
			selectString.Append($@"SELECT [t].[Id], [t].[Currency], [t].[Discription], [t].[IsRemoved], [t].[Price], [t].[PublicationDate], [t].[Title], [t].[Type],
						[aInPE].[Id], [aInPE].[AuthorId], [aInPE].[BookId], [aInPE].[aInBID], [aInPE].[FirstName], [aInPE].[IsRemoved], [aInPE].[LastName]
								FROM (
									SELECT [p].[Id], [p].[Currency], [p].[Discription], [p].[IsRemoved], [p].[Price], [p].[PublicationDate], [p].[Title], [p].[Type]
									FROM [PrintingEditions] AS [p]");
			selectString.Append($" WHERE([p].[Title] LIKE '%{filterDataModel.SearchFilter}%') ");

			foreach (var item in filterDataModel.FilterCheckBoxDataModels)
			{
				selectString.Append($" AND([p].[{item.FiltrationСolumn}] != {item.Value}) ");
			}
			selectString.Append($" AND (P.IsRemoved != 1) ");
			selectString.Append($" ORDER BY [p].{filterDataModel.SortColumnName.ToString()} {filterDataModel.SortType.ToString().ToUpper()} ");
			selectString.Append($" OFFSET {filterDataModel.PageModel.PageNumber * filterDataModel.PageModel.PageSize} ROWS FETCH NEXT {filterDataModel.PageModel.PageSize} ROWS ONLY ");
			selectString.Append($" ) AS[t] ");
			selectString.Append($@"  LEFT JOIN(
									  SELECT[a].[Id], [a].[AuthorId], [a].[BookId], [aInB].[Id] AS[aInBID], [aInB].[FirstName], [aInB].[IsRemoved], [aInB].[LastName]
										FROM[AuthorBooks] AS[a]
									  LEFT JOIN[Authors] AS[aInB] ON[a].[AuthorId] = [aInB].[Id]
									 ) AS[aInPE] ON[t].[Id] = [aInPE].[BookId] ");
			selectString.Append($" ORDER BY [t].{filterDataModel.SortColumnName.ToString()} {filterDataModel.SortType.ToString().ToUpper()} ");
			selectString.Append($@" Select Count(*) 
								FROM PrintingEditions AS P
								WHERE (P.Title LIKE '%{filterDataModel.SearchFilter}%')");
			selectString.Append($" AND (P.IsRemoved != 1) ");
			if (minPrice < maxPrice && minPrice != maxPrice)
			{
				selectString.Append($" AND([p].[Price] < {minPrice}) ");
				selectString.Append($" AND([p].[Price] > {maxPrice}) ");
			}
			foreach (var item in filterDataModel.FilterCheckBoxDataModels)
			{
				selectString.Append($" AND([p].[{item.FiltrationСolumn}] != {item.Value}) ");
			}
			var response = new FilterResponseDataModel<PrintingEdition>();
			var items = new List<PrintingEdition>();
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				var query = selectString.ToString();
				using (var multi = await dbConnection.QueryMultipleAsync(query))
				{
					var resultList = multi.Read<PrintingEdition, AuthorInPrintingEdition, Author, AuthorInPrintingEdition>(
						(PrintingEdition, AuthorBook, Author) =>
						{

							AuthorBook.PrintingEdition = PrintingEdition;
							AuthorBook.Author = Author;
							return AuthorBook;
						},
						splitOn: "Id, Id, aInBID"
						);
					items = resultList.GroupBy(p => p.PrintingEdition.Id).Select(x => new PrintingEdition()
					{
						AuthorBooks = x.Select(y => new AuthorInPrintingEdition()
						{
							Author = y.Author,
							PrintingEdition = y.PrintingEdition,
							Id = y.Id
						}).ToList()
					}).ToList();
					response.AllCount = multi.Read<int>().First();
				}
				var result = new List<PrintingEdition>();
				foreach (var item in items)
				{
					result.Add(new PrintingEdition()
					{
						Id = item.AuthorBooks.First().PrintingEdition.Id,
						AuthorBooks = item.AuthorBooks,
						Currency = item.AuthorBooks.First().PrintingEdition.Currency,
						Discription = item.AuthorBooks.First().PrintingEdition.Discription,
						IsRemoved = item.AuthorBooks.First().PrintingEdition.IsRemoved,
						Price = item.AuthorBooks.First().PrintingEdition.Price,
						PublicationDate = item.AuthorBooks.First().PrintingEdition.PublicationDate,
						Title = item.AuthorBooks.First().PrintingEdition.Title,
						Type = item.AuthorBooks.First().PrintingEdition.Type
					});
				}
				response.Items = result;

			}

			return response;

		}
	}
}
