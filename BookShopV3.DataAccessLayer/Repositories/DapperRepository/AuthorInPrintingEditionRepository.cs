﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BookShopV3.DataAccessLayer.Repositories.DapperRepository
{
	public class AuthorInPrintingEditionRepository : BaseDapperRepository<AuthorInPrintingEdition>, IAuthorInPrintingEditionRepository
	{
		public AuthorInPrintingEditionRepository(string connectionString) : base(connectionString)
		{
		}

		public List<AuthorInPrintingEdition> Find(Func<AuthorInPrintingEdition, bool> predicate)
		{
			var selectString = new StringBuilder();
			selectString.Append($@"
							SELECT *
							FROM dbo.AuthorInPrintingEditions AS AP
							INNER JOIN (
							SELECT *
							FROM dbo.PrintingEditions
							)AS P ON AP.BookId = P.Id
							INNER JOIN (
							SELECT *
							FROM dbo.Authors
							)AS A ON AP.AuthorId = A.Id
			");
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				var query = selectString.ToString();
				using (var multi = dbConnection.QueryMultiple(query))
				{
					var resultList = multi.Read<AuthorInPrintingEdition, PrintingEdition, Author, AuthorInPrintingEdition>(
						(AuthorInPrintingEdition, PrintingEdition, Author) =>
						{
							AuthorInPrintingEdition.Author = Author;
							AuthorInPrintingEdition.PrintingEdition = PrintingEdition;
							return AuthorInPrintingEdition;
						},
						splitOn: "Id, Id, Id"
						);
					resultList = resultList.Where(predicate);
					return resultList.ToList();
				}
			}
		}
	}
}
