﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.DapperRepository
{
	public class AthorRepository : BaseDapperRepository<Author>, IAuthorRepository
	{
		public AthorRepository(string connectionString) : base(connectionString) { }

		public async Task<FilterResponseDataModel<Author>> GetFilteredAsync(FilterRequestDataModel filterDataModel)
		{
			var selectString = new StringBuilder();
			selectString.Append($@"SELECT [t].[Id], [t].[FirstName], [t].[IsRemoved], [t].[LastName], [authorInPrintingEdition].[Id], [authorInPrintingEdition].[AuthorId], [authorInPrintingEdition].[BookId],
							[authorInPrintingEdition].[aInPEId], [authorInPrintingEdition].[Currency], [authorInPrintingEdition].[Discription], [authorInPrintingEdition].[IsRemoved], [authorInPrintingEdition].[Price], [authorInPrintingEdition].[PublicationDate], [authorInPrintingEdition].[Title], [authorInPrintingEdition].[Type]
							  FROM (
								  SELECT [a].[Id], [a].[FirstName], [a].[IsRemoved], [a].[LastName]
								  FROM [Authors] AS [a]
								  WHERE ([a].[FirstName] + N' ') + [a].[LastName] LIKE '%{filterDataModel.SearchFilter}%'
									AND ([a].IsRemoved != 1)
								  ORDER BY [a].[Id] 
								  OFFSET {filterDataModel.PageModel.PageNumber * filterDataModel.PageModel.PageSize} ROWS FETCH NEXT {filterDataModel.PageModel.PageSize} ROWS ONLY
							  ) AS [t]
							  LEFT JOIN (
								  SELECT [aInB].[Id], [aInB].[AuthorId], [aInB].[BookId], [p].[Id] AS [aInPEId], [p].[Currency], [p].[Discription], [p].[IsRemoved], [p].[Price], [p].[PublicationDate], [p].[Title], [p].[Type]
								  FROM [AuthorBooks] AS [aInB]
								  LEFT JOIN [PrintingEditions] AS [p] ON [aInB].[BookId] = [p].[Id]
							  ) AS [authorInPrintingEdition] ON [t].[Id] = [authorInPrintingEdition].[AuthorId]
							  ORDER BY [t].[Id] {filterDataModel.SortType.ToString().ToUpper()}, [authorInPrintingEdition].[Id]");

			selectString.Append($@" Select Count(*) 
								FROM Authors AS a
								 WHERE ([a].[FirstName] + N' ') + [a].[LastName] LIKE '%{filterDataModel.SearchFilter}%' ");
			selectString.Append($" AND (a.IsRemoved != 1) ");
			var response = new FilterResponseDataModel<Author>();
			var items = new List<Author>();
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				var query = selectString.ToString();
				using (var multi = await dbConnection.QueryMultipleAsync(query))
				{
					var resultList = multi.Read<Author, AuthorInPrintingEdition, PrintingEdition, AuthorInPrintingEdition>(
						(Author, AuthorBook, PrintingEdition) =>
						{
							AuthorBook.PrintingEdition = PrintingEdition;
							AuthorBook.Author = Author;
							return AuthorBook;
						},
						splitOn: "Id, Id, aInPEId"
						);
					items = resultList.GroupBy(p => p.Author.Id).Select(x => new Author()
					{
						AuthorBooks = x.Select(y => new AuthorInPrintingEdition()
						{
							Author = y.Author,
							PrintingEdition = y.PrintingEdition,
							Id = y.Id
						}).ToList()
					}).ToList();
					response.AllCount = multi.Read<int>().First();
				}
				var result = new List<Author>();
				foreach (var item in items)
				{
					result.Add(new Author()
					{
						AuthorBooks = item.AuthorBooks,
						FirstName = item.AuthorBooks.First().Author.FirstName,
						LastName = item.AuthorBooks.First().Author.LastName,
						Id = item.AuthorBooks.First().Author.Id
					});
				}
				response.Items = result;

			}

			return response;
		}


	}
}
