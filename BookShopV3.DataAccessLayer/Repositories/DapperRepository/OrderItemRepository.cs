﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;

namespace BookShopV3.DataAccessLayer.Repositories.DapperRepository
{
	public class OrderItemRepository : BaseDapperRepository<OrderItem>, IOrderItemRepository
	{
		public OrderItemRepository(string connectionString) : base(connectionString) { }

	}
}
