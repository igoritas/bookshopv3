﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Base
{
	public class BaseDapperRepository<T> : IBaseRepository<T> where T : BaseEntity
	{
		protected readonly string connectionString;
		public BaseDapperRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public async Task<T> CreateAsync(T item)
		{
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				await dbConnection.InsertAsync<T>(item);
			}
			return item;
		}

		public async Task<int> DeleteAsync(string id)
		{
			var entity = await GetByIdAsync(id);
			entity.IsRemoved = true;
			var isSuccess = false;
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				isSuccess = await dbConnection.UpdateAsync<T>(entity);
			}
			return Convert.ToInt32(isSuccess);
		}

		public async Task<List<T>> GetAllAsync()
		{
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				var allEntitys = await dbConnection.GetAllAsync<T>();
				return allEntitys.ToList();
			}
		}

		public async Task<T> GetByIdAsync(string id)
		{
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				var idEntity = new Guid(id);
				var entity = await dbConnection.GetAsync<T>(idEntity);
				return entity;
			}
		}

		public async Task<int> UpdateAsync(T item)
		{
			var isSuccess = false;
			using (IDbConnection dbConnection = new SqlConnection(connectionString))
			{
				isSuccess = await dbConnection.UpdateAsync<T>(item);
			}
			return Convert.ToInt32(isSuccess);
		}
	}
}
