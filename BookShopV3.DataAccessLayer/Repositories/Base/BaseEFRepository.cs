﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Base
{
	public class BaseEFRepository<T> : IBaseRepository<T> where T : BaseEntity
	{
		protected readonly ApplicationContext _context;
		public BaseEFRepository(ApplicationContext context)
		{
			this._context = context;
		}

		public async Task<T> CreateAsync(T item)
		{
			item.CreationDate = DateTime.UtcNow;
			var entity = _context.Entry(item);
			entity.State = EntityState.Added;
			await _context.SaveChangesAsync();
			return entity.Entity;
		}

		public async Task<int> DeleteAsync(string id)
		{
			var item = await GetByIdAsync(id);
			_context.Entry(item).State = EntityState.Deleted;
			return await _context.SaveChangesAsync();
		}
		public async Task<T> GetByIdAsync(string id)
		{
			return await _context.Set<T>()
				.AsNoTracking()
				.FirstOrDefaultAsync(e => e.Id.ToString() == id);
		}

		public async Task<List<T>> GetAllAsync()
		{
			return await _context.Set<T>()
				.AsNoTracking().ToListAsync();
		}

		public async Task<int> UpdateAsync(T item)
		{
			_context.Entry(item).State = EntityState.Modified;
			return await _context.SaveChangesAsync();
		}
	}
}
