﻿using BookShopV3.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Interfaces
{
	public interface IOrderItemRepository
	{
		Task<List<OrderItem>> GetAllAsync();
		Task<OrderItem> GetByIdAsync(string id);
		Task<OrderItem> CreateAsync(OrderItem item);
		Task<int> UpdateAsync(OrderItem item);
		Task<int> DeleteAsync(string id);
	}
}
