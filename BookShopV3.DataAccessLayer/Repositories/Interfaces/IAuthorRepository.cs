﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Interfaces
{
	public interface IAuthorRepository
	{
		Task<List<Author>> GetAllAsync();
		Task<Author> GetByIdAsync(string id);
		Task<FilterResponseDataModel<Author>> GetFilteredAsync(FilterRequestDataModel filterDataModel);
		Task<Author> CreateAsync(Author item);
		Task<int> UpdateAsync(Author item);
		Task<int> DeleteAsync(string id);

	}
}
