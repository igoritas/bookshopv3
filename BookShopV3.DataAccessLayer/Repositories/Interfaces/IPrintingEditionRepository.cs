﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Interfaces
{
	public interface IPrintingEditionRepository
	{
		Task<List<PrintingEdition>> GetAllAsync();
		Task<PrintingEdition> GetByIdAsync(string id);
		Task<PrintingEdition> CreateAsync(PrintingEdition item);
		Task<int> UpdateAsync(PrintingEdition item);
		Task<int> DeleteAsync(string id);
		Task<FilterResponseDataModel<PrintingEdition>> GetFilteredBookAsync(FilterRequestDataModel filterDataModel, decimal minPrice, decimal maxPrice);

	}
}
