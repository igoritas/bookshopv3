﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Interfaces
{
	public interface IUserRepository
	{
		Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role);
		Task<ApplicationUser> GetAsync(string id);
		Task<IdentityResult> UpdateAsync(ApplicationUser item);
		Task<FilterResponseDataModel<ApplicationUser>> GetFilteredUsersAsync(FilterRequestDataModel filterDataModel);
		Task<SignInResult> PasswordSignInAsync(string login, string password);
		Task<ApplicationUser> FindByLoginAsync(string login);
		Task SignOutAsync();
		Task<IdentityResult> CreateUserWithPassword(ApplicationUser user, string password);
		Task<ApplicationUser> FindByEmailAsync(string email);
		Task<string> GenerateEmailConfirmationTokenAsync(ApplicationUser user);
		Task SignInAsync(ApplicationUser user);
		Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string emailConfirmToken);
		Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user);
		Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string passwordResetToken, string newPassword);
		Task<ApplicationUser> FindByIdAsync(string id);
		Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword);
		Task<string> GenerateChangeEmailTokenAsync(ApplicationUser user);
		Task<IdentityResult> ChangeEmailAsync(ApplicationUser user, string changeEmailToken);
		Task<List<string>> GetRolesAsync(ApplicationUser user);
	}
}
