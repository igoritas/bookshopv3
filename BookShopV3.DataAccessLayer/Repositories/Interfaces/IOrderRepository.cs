﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Interfaces
{
	public interface IOrderRepository
	{
		Task<List<Order>> GetAllAsync();
		Task<Order> GetByIdAsync(string id);
		Task<Order> CreateAsync(Order item);
		Task<int> UpdateAsync(Order item);
		Task<int> DeleteAsync(string id);
		Task<FilterResponseDataModel<Order>> GetFilteredAsync(FilterRequestDataModel filterDataModel);

	}
}
