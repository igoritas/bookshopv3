﻿using BookShopV3.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Interfaces
{
	public interface IAuthorInPrintingEditionRepository
	{
		Task<List<AuthorInPrintingEdition>> GetAllAsync();
		Task<AuthorInPrintingEdition> GetByIdAsync(string id);
		List<AuthorInPrintingEdition> Find(Func<AuthorInPrintingEdition, Boolean> predicate);
		Task<AuthorInPrintingEdition> CreateAsync(AuthorInPrintingEdition item);
		Task<int> UpdateAsync(AuthorInPrintingEdition item);
		Task<int> DeleteAsync(string id);
	}
}
