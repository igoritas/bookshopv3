﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.Interfaces
{
	public interface IBaseRepository<T> where T : class
	{
		Task<T> CreateAsync(T item);
		Task<int> UpdateAsync(T item);
		Task<int> DeleteAsync(string id);
		Task<List<T>> GetAllAsync();
		Task<T> GetByIdAsync(string id);
	}
}
