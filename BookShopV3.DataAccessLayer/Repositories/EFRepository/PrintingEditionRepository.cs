﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.EFRepository
{
	public class PrintingEditionRepository : BaseEFRepository<PrintingEdition>, IPrintingEditionRepository
	{
		public PrintingEditionRepository(ApplicationContext context) : base(context) { }

		public async Task<FilterResponseDataModel<PrintingEdition>> GetFilteredBookAsync(FilterRequestDataModel filterDataModel, decimal minPrice, decimal maxPrice)
		{

			var filterResponse = new FilterResponseDataModel<PrintingEdition>();
			var query = _context.PrintingEditions.Include(pe => pe.AuthorBooks)
												.ThenInclude(a => a.Author)
												.Where(PE => EF.Functions.Like(PE.Title, $"%{filterDataModel.SearchFilter}%"))
												.AsQueryable();

			query = query.OrderBy($"{filterDataModel.SortColumnName.ToString()} {filterDataModel.SortType.ToString().ToUpper()}");
			foreach (var item in filterDataModel.FilterCheckBoxDataModels)
			{
				query = query.Where($"{item.FiltrationСolumn} != {item.Value}");
			}
			if (minPrice < maxPrice && minPrice != maxPrice)
			{
				query = query.Where(x => x.Price > minPrice && x.Price < maxPrice);
			}
			query = query.Where(b => !b.IsRemoved);
			filterResponse.AllCount = await query.CountAsync();
			filterResponse.Items = await query.Skip(filterDataModel.PageModel.PageNumber * filterDataModel.PageModel.PageSize).Take(filterDataModel.PageModel.PageSize).ToListAsync();
			return filterResponse;
		}

		public new async Task<int> DeleteAsync(string id)
		{
			var item = await GetByIdAsync(id);
			_context.AuthorInPrintingEditions.RemoveRange(item.AuthorBooks);
			item.IsRemoved = true;
			_context.Entry(item).State = EntityState.Modified;
			return await _context.SaveChangesAsync();
		}
		public new async Task<PrintingEdition> GetByIdAsync(string id)
		{
			return await _context.PrintingEditions.AsNoTracking()
													.Include(ab => ab.AuthorBooks)
													.ThenInclude(b => b.Author)
													.FirstOrDefaultAsync(a => a.Id.ToString() == id);
		}
	}


}
