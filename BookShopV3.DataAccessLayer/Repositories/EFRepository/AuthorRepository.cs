﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.EFRepository
{
	public class AuthorRepository : BaseEFRepository<Author>, IAuthorRepository
	{
		public AuthorRepository(ApplicationContext context) : base(context)
		{ }
		public async Task<FilterResponseDataModel<Author>> GetFilteredAsync(FilterRequestDataModel filterDataModel)
		{
			var filterResponse = new FilterResponseDataModel<Author>();
			var query = _context.Authors
								.Include(ab => ab.AuthorBooks)
								.ThenInclude(b => b.PrintingEdition)
								.Where(p => EF.Functions.Like(p.FirstName + " " + p.LastName, $"%{filterDataModel.SearchFilter}%"))
								.AsQueryable();
			query = query.OrderBy($"{filterDataModel.SortColumnName.ToString()} {filterDataModel.SortType.ToString().ToUpper()}");
			query = query.Where(a => !a.IsRemoved);
			filterResponse.AllCount = await query.CountAsync();
			filterResponse.Items = await query.Skip(filterDataModel.PageModel.PageNumber * filterDataModel.PageModel.PageSize).Take(filterDataModel.PageModel.PageSize).ToListAsync();
			return filterResponse;
		}
		public new async Task<Author> GetByIdAsync(string id)
		{
			return await _context.Authors.AsNoTracking()
											.Include(ab => ab.AuthorBooks)
											.ThenInclude(b => b.PrintingEdition)
											.FirstOrDefaultAsync(a => a.Id.ToString() == id);
		}
		public new async Task<int> DeleteAsync(string id)
		{
			var item = await GetByIdAsync(id);
			item.IsRemoved = true;
			_context.AuthorInPrintingEditions.RemoveRange(item.AuthorBooks);
			_context.Entry(item).State = EntityState.Modified;
			return await _context.SaveChangesAsync();
		}
	}
}
