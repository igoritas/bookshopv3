﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.EFRepository
{
	public class UserRepository : IUserRepository
	{
		private readonly ApplicationContext _context;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;

		public UserRepository(ApplicationContext context,
			UserManager<ApplicationUser> userManager,
			SignInManager<ApplicationUser> signInManager
			)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_context = context;
		}

		public async Task<SignInResult> PasswordSignInAsync(string login, string password)
			=> await _signInManager.PasswordSignInAsync(login, password, false, false);

		public async Task<ApplicationUser> FindByLoginAsync(string login)
			=> await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == login);

		public async Task<ApplicationUser> FindByEmailAsync(string email)
			=> await _userManager.Users.FirstOrDefaultAsync(x => x.Email == email);

		public async Task<IdentityResult> CreateUserWithPassword(ApplicationUser user, string password)
			=> await _userManager.CreateAsync(user, password);
		public async Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role)
			=> await _userManager.AddToRoleAsync(user, role);
		public async Task<FilterResponseDataModel<ApplicationUser>> GetFilteredUsersAsync(FilterRequestDataModel filterDataModel)
		{
			var filterResponse = new FilterResponseDataModel<ApplicationUser>();
			var query = _context.ApplicationUsers.Include(pe => pe.Orders)
												.Where(ap => EF.Functions.Like(ap.FirstName + " " + ap.LastName, $"%{filterDataModel.SearchFilter}%"))
												.AsQueryable();
			query = query.OrderBy($"{filterDataModel.SortColumnName.ToString()} {filterDataModel.SortType.ToString().ToUpper()}");
			foreach (var item in filterDataModel.FilterCheckBoxDataModels)
			{
				query = query.Where($"{item.FiltrationСolumn} != (@0)", Convert.ToBoolean(item.Value).ToString());
			}
			filterResponse.AllCount = query.Count();
			filterResponse.Items = await query.Skip(filterDataModel.PageModel.PageNumber * filterDataModel.PageModel.PageSize).Take(filterDataModel.PageModel.PageSize).ToListAsync();
			return filterResponse;
		}

		public async Task<ApplicationUser> GetAsync(string id)
			=> await _context.Users.AsNoTracking()
									.FirstOrDefaultAsync(b => b.Id == id.ToString());

		public async Task<ApplicationUser> GetAsync(Expression<Func<ApplicationUser, bool>> predicate)
			=> await _context.ApplicationUsers.AsNoTracking()
												.FirstOrDefaultAsync(predicate);

		public async Task<IdentityResult> UpdateAsync(ApplicationUser item)
			=> await _userManager.UpdateAsync(item);

		public async Task SignOutAsync()
			=> await _signInManager.SignOutAsync();

		public async Task<string> GenerateEmailConfirmationTokenAsync(ApplicationUser user)
			=> await _userManager.GenerateEmailConfirmationTokenAsync(user);

		public async Task SignInAsync(ApplicationUser user)
			=> await _signInManager.SignInAsync(user, false);

		public async Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string emailConfirmToken)
			=> await _userManager.ConfirmEmailAsync(user, emailConfirmToken);

		public async Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user)
			=> await _userManager.GeneratePasswordResetTokenAsync(user);

		public async Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string passwordResetToken, string newPassword)
			=> await _userManager.ResetPasswordAsync(user, passwordResetToken, newPassword);

		public async Task<ApplicationUser> FindByIdAsync(string id) => await _userManager.FindByIdAsync(id);

		public async Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword)
			=> await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);

		public async Task<string> GenerateChangeEmailTokenAsync(ApplicationUser user)
			=> await _userManager.GenerateChangeEmailTokenAsync(user, user.Email);

		public Task<IdentityResult> ChangeEmailAsync(ApplicationUser user, string changeEmailToken)
			=> _userManager.ChangeEmailAsync(user, user.Email, changeEmailToken);

		public async Task<List<string>> GetRolesAsync(ApplicationUser user)
			=> await _userManager.GetRolesAsync(user) as List<string>;
	}
}
