﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.EFRepository
{
	public class AuthorInPrintingEditionRepository : BaseEFRepository<AuthorInPrintingEdition>, IAuthorInPrintingEditionRepository
	{
		public AuthorInPrintingEditionRepository(ApplicationContext _context) : base(_context)
		{ }

		public List<AuthorInPrintingEdition> Find(Func<AuthorInPrintingEdition, bool> predicate)
		{
			var result = _context.AuthorInPrintingEditions
									.AsNoTracking()
									.Include(ab => ab.Author)
									 .Include(ab => ab.PrintingEdition)
									 .Where(predicate)
									 .ToList();
			return result;

		}

		new public async Task<IEnumerable<AuthorInPrintingEdition>> GetAllAsync()
		{
			return await _context.AuthorInPrintingEditions.AsNoTracking().Include(ab => ab.Author)
									 .Include(ab => ab.PrintingEdition).ToListAsync();
		}

	}
}
