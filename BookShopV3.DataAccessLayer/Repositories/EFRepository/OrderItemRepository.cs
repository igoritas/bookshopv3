﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;

namespace BookShopV3.DataAccessLayer.Repositories.EFRepository
{
	public class OrderItemRepository : BaseEFRepository<OrderItem>, IOrderItemRepository
	{
		public OrderItemRepository(ApplicationContext context) : base(context)
		{
		}

	}
}
