﻿using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Models;
using BookShopV3.DataAccessLayer.Repositories.Base;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace BookShopV3.DataAccessLayer.Repositories.EFRepository
{
	public class OrderRepository : BaseEFRepository<Order>, IOrderRepository
	{
		public OrderRepository(ApplicationContext context) : base(context)
		{ }

		new public async Task<Order> CreateAsync(Order item)
		{
			item.CreationDate = DateTime.UtcNow;
			item.Payment.CreationDate = DateTime.UtcNow;
			_context.Entry(item).State = EntityState.Added;
			_context.Entry(item.Payment).State = EntityState.Added;
			await _context.SaveChangesAsync();
			return item;
		}

		public new async Task<int> DeleteAsync(string id)
		{
			var order = await _context.Orders.FirstOrDefaultAsync(o => o.Id.ToString() == id);
			order.IsRemoved = true;
			_context.Entry(order).State = EntityState.Modified;
			return await _context.SaveChangesAsync();
		}

		public new async Task<Order> GetByIdAsync(string id)
		{
			return await _context.Orders.AsNoTracking()
										.Include(p => p.Payment)
										.Include(u => u.ApplicationUser)
										.Include(b => b.OrderItems)
										.ThenInclude(oi => oi.Book)
										.FirstOrDefaultAsync(b => b.Id.ToString() == id);
		}
		public async Task<FilterResponseDataModel<Order>> GetFilteredAsync(FilterRequestDataModel filterDataModel)
		{

			var filterResponse = new FilterResponseDataModel<Order>();
			var query = _context.Orders.Include(pe => pe.OrderItems)
										.ThenInclude(b => b.Book)
										.Include(p => p.Payment)
										.Include(a => a.ApplicationUser)
										.AsQueryable();
			query = query.OrderBy($"{filterDataModel.SortColumnName.ToString()} {filterDataModel.SortType.ToString().ToUpper()}");
			foreach (var item in filterDataModel.FilterCheckBoxDataModels)
			{
				query = query.Where($"{item.FiltrationСolumn.ToString()} != {item.Value}");
			}
			query = query.Where(b => !b.IsRemoved);
			filterResponse.AllCount = await query.CountAsync();
			filterResponse.Items = await query.Skip(filterDataModel.PageModel.PageNumber * filterDataModel.PageModel.PageSize).Take(filterDataModel.PageModel.PageSize).ToListAsync();
			return filterResponse;
		}
	}
}
