﻿using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace BookShopV3.DataAccessLayer.Entities
{
	public class ApplicationUser : IdentityUser
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		[Computed]
		public IEnumerable<Order> Orders { get; set; }
		public bool IsActive { get; set; }
		public ApplicationUser()
		{
			Orders = new List<Order>();
		}
	}
}
