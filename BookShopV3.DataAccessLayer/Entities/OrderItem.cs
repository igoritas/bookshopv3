﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookShopV3.DataAccessLayer.Entities
{
	public class OrderItem : BaseEntity
	{
		public PrintingEdition Book { get; set; }
		public int Count { get; set; }
		public decimal Amount { get; set; }
		public Guid OrderId { get; set; }
		[ForeignKey("OrderId")]
		public Order Order { get; set; }
	}
}
