﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BookShopV3.DataAccessLayer.Entities
{
	public class Author : BaseEntity
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		[Computed]
		public List<AuthorInPrintingEdition> AuthorBooks { get; set; }
		public Author()
		{
			AuthorBooks = new List<AuthorInPrintingEdition>();
		}
	}
}
