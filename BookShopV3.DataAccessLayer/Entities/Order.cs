﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookShopV3.DataAccessLayer.Entities
{
	public class Order : BaseEntity
	{
		public decimal Summ { get; set; }
		public string ApplicationUserId { get; set; }
		[ForeignKey("ApplicationUserId")]
		public ApplicationUser ApplicationUser { get; set; }
		public List<OrderItem> OrderItems { get; set; }
		public Currency Currency { get; set; }
		[Computed]
		public Guid PaymentId { get; set; }
		[ForeignKey("PaymentId")]
		public Payment Payment { get; set; }
		public PayStatus PayStatus { get; set; }
	}
}

