﻿using Dapper.Contrib.Extensions;
using System;

namespace BookShopV3.DataAccessLayer.Entities
{
	public class BaseEntity
	{
		private Guid _id;
		[Computed]
		public Guid Id
		{
			get
			{
				if (_id == Guid.Empty)
				{
					_id = Guid.NewGuid();
				}
				return _id;
			}
			set
			{
				_id = value;
			}
		}
		public bool IsRemoved { get; set; }
		public DateTime CreationDate { get; set; }

	}
}
