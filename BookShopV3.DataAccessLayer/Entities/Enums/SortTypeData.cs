﻿namespace BookShopV3.DataAccessLayer.Enums
{
	public enum SortType
	{
		asc = 0,
		desc = 1
	}
}
