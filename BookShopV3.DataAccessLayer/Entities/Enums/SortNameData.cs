﻿namespace BookShopV3.DataAccessLayer.Enums
{
	public enum SortColumnName
	{
		Id = 0,
		Order = 1,
		CreationDate = 2,
		Summ = 3,
		Price = 4
	}
}
