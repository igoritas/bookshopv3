﻿namespace BookShopV3.DataAccessLayer.Entities
{
	public class AuthorInPrintingEdition : BaseEntity
	{
		public Author Author { get; set; }
		public PrintingEdition PrintingEdition { get; set; }
	}
}
