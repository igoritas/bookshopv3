﻿using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookShopV3.DataAccessLayer.Entities
{
	public class Payment : BaseEntity
	{
		public string TransactionId { get; set; }

		[NotMapped]
		[Computed]
		public Guid OrderId { get; set; }

		public Order Order { get; set; }
	}
}
