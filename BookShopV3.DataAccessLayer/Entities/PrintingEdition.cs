﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;

namespace BookShopV3.DataAccessLayer.Entities
{
	public class PrintingEdition : BaseEntity
	{
		public string Title { get; set; }
		public decimal Price { get; set; }
		public DateTime PublicationDate { get; set; }
		public Currency Currency { get; set; }
		public PrintingEditionType Type { get; set; }
		[Computed]
		public List<AuthorInPrintingEdition> AuthorBooks { get; set; }
		public string Discription { get; set; }
		public PrintingEdition()
		{
			AuthorBooks = new List<AuthorInPrintingEdition>();
		}
	}
}
