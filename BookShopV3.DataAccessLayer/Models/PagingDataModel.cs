﻿namespace BookShopV3.DataAccessLayer.Models
{
	public class PagingDataModel
	{
		public int PageNumber { get; set; } = 0;
		public int PageSize { get; set; }
	}
}