﻿using System.Collections.Generic;

namespace BookShopV3.DataAccessLayer.Models
{
	public class FilterResponseDataModel<T> where T : class
	{
		public IEnumerable<T> Items { get; set; }
		public int AllCount { get; set; }
		public FilterResponseDataModel()
		{
			Items = new List<T>();
		}
	}
}
