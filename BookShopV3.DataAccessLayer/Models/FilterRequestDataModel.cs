﻿using BookShopV3.DataAccessLayer.Enums;
using System.Collections.Generic;

namespace BookShopV3.DataAccessLayer.Models
{
	public class FilterRequestDataModel
	{
		public string SearchFilter { get; set; }
		public SortColumnName SortColumnName { get; set; }
		public SortType SortType { get; set; }
		public PagingDataModel PageModel { get; set; }
		public List<FilterDataModel> FilterCheckBoxDataModels { get; set; }
		public FilterRequestDataModel()
		{
			FilterCheckBoxDataModels = new List<FilterDataModel>();
			PageModel = new PagingDataModel();
		}
	}
}
