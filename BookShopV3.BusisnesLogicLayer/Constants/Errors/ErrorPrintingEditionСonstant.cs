﻿namespace BookShopV3.BusinessLogicLayer.Constant.Error
{
	public partial class BusinessLogicLayerСonstant
	{
		public partial class ErrorСonstant
		{
			public class ErrorPrintingEditionСonstant
			{
				public const string Create = "create PrintingEdition failed";
				public const string Update = "PrintingEdition is not updated";
				public const string Delete = "PrintingEdition is not delete";
				public const string Filtring = "PrintingEdition filtering error";
			}
		}
	}
}
