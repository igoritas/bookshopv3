﻿namespace BookShopV3.BusinessLogicLayer.Constant.Error
{
	public partial class BusinessLogicLayerСonstant
	{
		public partial class ErrorСonstant
		{
			public class ErrorOrderСonstant
			{
				public const string Create = "create Order failed";
				public const string Update = "Order is not updated";
				public const string Delete = "Order is not delete";
				public const string Filtring = "Order filtering error";
			}
		}
	}
}
