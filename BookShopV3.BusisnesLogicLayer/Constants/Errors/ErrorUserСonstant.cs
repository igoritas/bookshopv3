﻿namespace BookShopV3.BusinessLogicLayer.Constant.Error
{
	public partial class BusinessLogicLayerСonstant
	{
		public partial class ErrorСonstant
		{
			public class ErrorUserСonstant
			{
				public const string Create = "create User failed";
				public const string Update = "User is not updated";
				public const string Delete = "User is not delete";
				public const string Filtring = "User filtering error";
				public const string NoSuchUser = "there is no such user";
				public const string IsNotActive = "User is not activated";

			}
		}
	}
}
