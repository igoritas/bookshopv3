﻿namespace BookShopV3.BusinessLogicLayer.Constant.Error
{
	public partial class BusinessLogicLayerСonstant
	{
		public partial class ErrorСonstant
		{
			public class ErrorAuthorСonstant
			{
				public const string Create = "create author failed";
				public const string Update = "author is not updated";
				public const string Delete = "author is not delete";
				public const string Filtring = "author filtering error";
			}
		}
	}
}
