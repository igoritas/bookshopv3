﻿namespace BookShopV3.BusinessLogicLayer.Constant.Information
{
	public partial class BusinessLogicLayerСonstant
	{
		public partial class InformationСonstant
		{
			public class InfoEmailConstant
			{
				public const string ConfimEmailBody = "Confirm the registration by clicking on the link:";
				public const string ConfimEmailHeader = "Confirm your account";
			}
		}
	}
}
