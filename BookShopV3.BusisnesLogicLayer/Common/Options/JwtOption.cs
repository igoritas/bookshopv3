﻿namespace BookShopV3.BusisnesLogicLayer.Common
{
	public class JwtOption
	{
		public string JwtKey { get; set; }
		public int JwtExpireDays { get; set; }
		public string JwtIssuer { get; set; }
		public string JwtAudienc { get; set; }
	}
}