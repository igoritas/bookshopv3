﻿namespace BookShopV3.BusisnesLogicLayer.Common
{
	public class EmailOption
	{
		public string FromAddress { get; set; }
		public string DisplayName { get; set; }
		public string Host { get; set; }
		public int Port { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }

	}
}
