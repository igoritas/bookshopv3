﻿using BookShopV3.BusisnesLogicLayer.Interfaces;
using NLog;

namespace BookShopV3.BusisnesLogicLayer.Services
{
	public class LoggerManager : ILoggerService
	{
		private readonly Logger logger = LogManager.GetCurrentClassLogger();

		public void LogError(string Error) =>
			logger.Error(Error);

		public void LogInfo(string Info) =>
			logger.Info(Info);
	}
}
