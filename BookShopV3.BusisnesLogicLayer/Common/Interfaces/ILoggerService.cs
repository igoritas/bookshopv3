﻿namespace BookShopV3.BusisnesLogicLayer.Interfaces
{
	public interface ILoggerService
	{
		void LogError(string Error);
		void LogInfo(string Info);
	}
}
