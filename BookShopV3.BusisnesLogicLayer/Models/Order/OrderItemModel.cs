﻿using BookShopV3.DataAccessLayer.Entities;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Models
{
	public class OrderItemModel
	{
		public string Id { get; set; }
		public PrintingEditionWithAuthorModel Book { get; set; }
		public int Count { get; set; }

		public decimal Price { get; set; }

		public OrderItemModel()
		{
			Book = new PrintingEditionWithAuthorModel();
		}
		public OrderItemModel(OrderItem orderItem)
		{
			Id = orderItem.Id.ToString();
			Count = orderItem.Count;
			Price = orderItem.Amount;
			Book = new PrintingEditionWithAuthorModel(orderItem.Book);
		}
	}
}
