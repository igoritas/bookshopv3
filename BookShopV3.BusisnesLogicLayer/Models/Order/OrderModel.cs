﻿using BookShopV3.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Models
{
	public class OrderModel : BaseModel
	{
		public string Id { get; set; }
		public Currency Currency { get; set; }
		public decimal TotalPrice { get; set; }
		public List<OrderItemModel> OrderItems { get; set; }
		public ApplicationUser User { get; set; }
		public string TransactionId { get; set; }
		public PayStatus IsPaid { get; set; }
		public DateTime Date { get; set; }

		public OrderModel()
		{
			OrderItems = new List<OrderItemModel>();
		}
		public OrderModel(Order order) : this()
		{
			Id = order.Id.ToString();
			TotalPrice = order.Summ;
			IsPaid = order.PayStatus;
			Currency = order.Currency;
			Date = order.CreationDate;
			if (order.Payment is Payment)
			{
				TransactionId = (order.Payment.TransactionId != null) ? order.Payment.TransactionId : string.Empty;
			}
			if (order.ApplicationUser is ApplicationUser)
			{
				User = new ApplicationUser() { UserName = order.ApplicationUser.UserName, Email = order.ApplicationUser.Email };

			}
			if (order.OrderItems is List<OrderItem>)
			{
				OrderItems = order.OrderItems.Select(x => new OrderItemModel(x)).ToList();
			}
		
		}
	}
}
