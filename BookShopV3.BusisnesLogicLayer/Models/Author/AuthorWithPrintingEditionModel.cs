﻿using BookShopV3.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Models
{
	public class AuthorWithPrintingEditionModel : BaseModel
	{
		public string Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public List<PrintingEditionWithAuthorModel> PrintintEditions { get; set; }
		public AuthorWithPrintingEditionModel()
		{
			PrintintEditions = new List<PrintingEditionWithAuthorModel>();
		}
	
		public AuthorWithPrintingEditionModel(Author author) : this()
		{
			Id = author.Id.ToString();
			FirstName = author.FirstName;
			LastName = author.LastName;
		}

	}
}
