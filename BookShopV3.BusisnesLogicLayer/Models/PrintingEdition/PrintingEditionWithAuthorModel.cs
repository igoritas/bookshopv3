﻿using BookShopV3.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Models
{
	public class PrintingEditionWithAuthorModel : BaseModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public PrintingEditionModelType Type { get; set; }
		public decimal Price { get; set; }
		public Currency Currency { get; set; }
		public DateTime PublicationDate { get; set; }
		public List<AuthorWithPrintingEditionModel> Authors { get; set; }
		public string Discription { get; set; }
		public PrintingEditionWithAuthorModel()
		{
			Authors = new List<AuthorWithPrintingEditionModel>();
		}
		public PrintingEditionWithAuthorModel(PrintingEdition book) : this()
		{
			Id = book.Id.ToString();
			Name = book.Title;
			Price = book.Price;
			Currency = book.Currency;
			PublicationDate = book.PublicationDate;
			Discription = book.Discription;
			Type = (PrintingEditionModelType)(int)book.Type;
			
		}
	}
}
