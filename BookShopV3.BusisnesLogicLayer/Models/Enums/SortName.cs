﻿namespace BookShopV3.BusisnesLogicLayer.Helpers.Enum
{
	public enum SortName
	{
		Id = 0,
		Order = 1,
		Date = 2,
		OrderPrice = 3,
		Price = 4
	}
}