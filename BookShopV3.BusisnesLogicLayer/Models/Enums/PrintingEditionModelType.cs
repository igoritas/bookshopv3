﻿public enum PrintingEditionModelType
{
	Book = 0,
	Journal = 1,
	Newspaper = 2
}