﻿namespace BookShopV3.BusisnesLogicLayer.Models.Enums
{
	public enum RoleType
	{
		None = 0,
		Admin = 1,
		User = 2
	}
}
