﻿namespace BookShopV3.BusisnesLogicLayer.Helpers.Enum
{
	public enum SortType
	{
		Asc = 0,
		Desc = 1
	}
}
