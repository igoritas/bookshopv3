﻿using BookShopV3.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Models
{
	public class UserModel : BaseModel
	{
		public string Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public bool EmailConfirmed { get; set; }
		public string Email { get; set; }
		public bool IsActive { get; set; }
		public List<string> Roles { get; set; }
		public List<OrderModel> Orders { get; set; }
		public UserModel() : base()
		{
			Roles = new List<string>();
			Orders = new List<OrderModel>();
		}

		public UserModel(ApplicationUser user) : this()
		{
			Id = user.Id.ToString();
			FirstName = user.FirstName;
			LastName = user.LastName;
			EmailConfirmed = user.EmailConfirmed;
			Email = user.Email;
			IsActive = user.IsActive;
			EmailConfirmed = user.EmailConfirmed;
			Orders = user.Orders.Select(x => new OrderModel(x)).ToList();
		}
	}
}
