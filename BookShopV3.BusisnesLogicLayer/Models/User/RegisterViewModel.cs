﻿namespace BookShopV3.BusisnesLogicLayer.Models
{
	public class RegisterViewModel
	{
		public string Login { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string PasswordConfirm { get; set; }
	}
}
