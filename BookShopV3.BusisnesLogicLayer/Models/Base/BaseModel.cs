﻿using System.Collections.Generic;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Models
{
	public class BaseModel
	{
		public List<string> Errors { get; set; }
		public BaseModel()
		{
			Errors = new List<string>();
		}
	}
}
