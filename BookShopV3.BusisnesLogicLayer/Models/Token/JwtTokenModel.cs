﻿namespace BookShopV3.BusisnesLogicLayer.Models.Token
{
	public class JwtTokenModel
	{
		public string AccessToken { get; set; }
		public string RefreshToken { get; set; }
	}
}
