﻿using System.Collections.Generic;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel
{
	public class FilterResponseModel<T> where T : class
	{
		public List<string> Errors { get; set; }
		public List<T> Items { get; set; }
		public int AllCount { get; set; }
		public FilterResponseModel()
		{
			Items = new List<T>();
			Errors = new List<string>();
		}
	}
}
