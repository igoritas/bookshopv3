﻿using BookShopV3.BusisnesLogicLayer.Helpers.Enum;
using System.Collections.Generic;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel
{
	public class FilterRequestModel
	{
		public string SearchString { get; set; }
		public SortName SortName { get; set; }
		public SortType SortType { get; set; }
		public PaginationModel PaginationModel { get; set; }
		public List<FilterCheckBox> FilterItems { get; set; }
		public FilterRequestModel()
		{
			FilterItems = new List<FilterCheckBox>();
		}
	}
}
