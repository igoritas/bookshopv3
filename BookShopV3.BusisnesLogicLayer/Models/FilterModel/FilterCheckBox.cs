﻿namespace BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel
{
	public class FilterCheckBox
	{
		public ColumnDatabaseFilter ColumnName { get; set; }
		public int Value { get; set; }
	}
}
