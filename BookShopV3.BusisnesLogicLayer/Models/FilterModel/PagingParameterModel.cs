﻿namespace BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel
{
	public class PaginationModel
	{
		public int PageNumber { get; set; } = 0;
		public int PageSize { get; set; }

	}
}
