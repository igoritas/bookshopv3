﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;

namespace BookShopV3.BusisnesLogicLayer.Models.FilterModel
{
	public class FilterPrintingEditionModel : FilterRequestModel
	{
		public decimal MinPrice { get; set; }
		public decimal MaxPrice { get; set; }
	}
}
