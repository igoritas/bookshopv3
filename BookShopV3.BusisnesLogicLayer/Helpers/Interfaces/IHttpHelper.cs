﻿using System.Threading.Tasks;

namespace BookShopV3.BusinessLogicLayer.Helpers.Interfaces
{
	public interface IHttpHelper
	{
		Task<string> GetHttpContent(string url);
	}
}
