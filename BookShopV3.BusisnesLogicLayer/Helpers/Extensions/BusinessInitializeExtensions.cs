﻿using BookShopV3.BusinessLogicLayer.Helpers.Interfaces;
using BookShopV3.BusisnesLogicLayer.Common;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using BookShopV3.BusisnesLogicLayer.Services;
using BookShopV3.DataAccessLayer.Initializer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Extensions
{
	public static class BusinessInitializeExtensions
	{
		public static void ServiceInitialiaz(this IServiceCollection services, IConfiguration configuration)
		{
			services.RepositoryInitialiaz(configuration);

			services.AddTransient<JWTHelpers>();

			services.AddTransient<IPrintingEditionService, PrintingEditionService>();

			services.AddTransient<IOrderService, OrderService>();

			services.AddTransient<IAuthorService, AuthorService>();

			services.AddTransient<IUserService, UserService>();

			services.AddTransient<IEmailService, EmailHelper>();

			services.AddTransient<ILoggerService, LoggerManager>();

			services.Configure<JwtOption>(configuration.GetSection(nameof(JwtOption)));

			services.AddSingleton<Logger>();

			services.Configure<ConvertCurrencyOption>(configuration.GetSection(nameof(ConvertCurrencyOption)));

			services.Configure<EmailOption>(configuration.GetSection(nameof(EmailOption)));

			services.Configure<GeneratePasswordOption>(configuration.GetSection(nameof(GeneratePasswordOption)));

			services.AddTransient<IHttpHelper, HttpHelper>();

		}
	}
}
