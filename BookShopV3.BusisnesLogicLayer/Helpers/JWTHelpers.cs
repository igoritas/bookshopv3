﻿using BookShopV3.BusisnesLogicLayer.Common;
using BookShopV3.BusisnesLogicLayer.Models.Token;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace BookShopV3.BusisnesLogicLayer.Helpers
{
	public class JWTHelpers
	{
		private readonly IOptions<JwtOption> _jwtOptions;
		public JWTHelpers(IOptions<JwtOption> jwtOptions)
		{
			_jwtOptions = jwtOptions;
		}

		public string GenerateRefreshToken()
		{
			string refreshToken = string.Empty;
			var randomNumber = new byte[32];
			using (var rng = RandomNumberGenerator.Create())
			{
				rng.GetBytes(randomNumber);
				refreshToken = Convert.ToBase64String(randomNumber);
			}
			return refreshToken;

		}
		public string GenerateJwtToken(string userName, string userId)
		{
			var claims = new List<Claim>
				{
				new Claim(JwtRegisteredClaimNames.Sub, userName),
				new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
				new Claim(ClaimTypes.NameIdentifier, userId)
				};
			return GenerateJwtToken(claims: claims);
		}
		public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
		{
			var tokenValidationParameters = new TokenValidationParameters
			{
				ValidateAudience = false,
				ValidateIssuer = false,
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Value.JwtKey)),
				ValidateLifetime = false,
			};

			var tokenHandler = new JwtSecurityTokenHandler();
			IdentityModelEventSource.ShowPII = true;
			SecurityToken securityToken;
			var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
			var jwtSecurityToken = securityToken as JwtSecurityToken;
			if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
			{
				throw new SecurityTokenException("Invalid token");
			}

			return principal;
		}
		public JwtTokenModel GenerateAccessAndRefreshToken(string userName, string userId)
		{
			var tokens = new JwtTokenModel();
			tokens.AccessToken = this.GenerateJwtToken(userName, userId);
			tokens.RefreshToken = this.GenerateRefreshToken();
			return tokens;
		}
		public string GenerateJwtToken(IEnumerable<Claim> claims)
		{
			var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Value.JwtKey));
			var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
			var expires = DateTime.Now.AddDays(Convert.ToDouble(_jwtOptions.Value.JwtExpireDays));
			var jwt = new JwtSecurityToken(
				_jwtOptions.Value.JwtIssuer,
				_jwtOptions.Value.JwtIssuer,
				claims: claims,
				expires: expires,
				signingCredentials: creds
			);
			return new JwtSecurityTokenHandler().WriteToken(jwt);
		}

	}
}
