﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using printingEditionM = BookShopV3.BusisnesLogicLayer.Helpers.Mapper.PrtintingEdition.CustomerMapper.PrintingEditionMapper;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Mapper.AuthorMapper
{
	public static partial class CustomerMapper
	{
		public class AuthorMapper
		{
			static public Author AuthorModelToEntity(AuthorWithPrintingEditionModel author)
			{
				var authorEntity = new Author();
				authorEntity.Id = (!string.IsNullOrWhiteSpace(author.Id)) ? new Guid(author.Id) : new Guid();
				authorEntity.FirstName = author.FirstName;
				authorEntity.LastName = author.LastName;
				return authorEntity;
			}
			
			static public Author AuthorWithPrintingEditionModelToEntity(AuthorWithPrintingEditionModel author)
			{
				var tmp = new List<AuthorInPrintingEdition>();
				tmp = author.PrintintEditions.Select(x => new AuthorInPrintingEdition()
				{
					Author = AuthorModelToEntity(author),
					PrintingEdition = printingEditionM.PrintingEditionModelToEntity(x)
				}).ToList();
				var authorResult = new Author()
				{
					AuthorBooks = tmp,
					Id = (!string.IsNullOrWhiteSpace(author.Id)) ? new Guid(author.Id) : new Guid(),
					FirstName = author.FirstName,
					LastName = author.LastName
				};
				return authorResult;
			}
			static public AuthorWithPrintingEditionModel AuthorEntityToAuthorModel(Author author)
			{
				var result = new AuthorWithPrintingEditionModel(author);
				result.PrintintEditions = author.AuthorBooks.Select(x => new PrintingEditionWithAuthorModel(x.PrintingEdition)).ToList();
				return result;
			}

		}

	}
}
