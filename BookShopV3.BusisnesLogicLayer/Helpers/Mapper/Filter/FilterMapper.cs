﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.DataAccessLayer.Enums;
using BookShopV3.DataAccessLayer.Models;
using System.Linq;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter
{
	public static partial class CustomerMapper
	{
		public class FilterMapper
		{
			static public FilterRequestDataModel FilterRequesModelToFilterDataModel(FilterRequestModel filterRequestModel)
			{
				
				var filterDataModel = new FilterRequestDataModel();
				filterDataModel.SearchFilter = filterRequestModel.SearchString;
				filterDataModel.SortColumnName = (SortColumnName)filterRequestModel.SortName;
				filterDataModel.SortType = (SortType)filterRequestModel.SortType;
				filterDataModel.PageModel.PageNumber = filterRequestModel.PaginationModel.PageNumber;
				filterDataModel.PageModel.PageSize = filterRequestModel.PaginationModel.PageSize;
				filterDataModel.FilterCheckBoxDataModels = filterRequestModel.FilterItems.Select(x => new FilterDataModel()
				{
					FiltrationСolumn = x.ColumnName,
					Value = x.Value
				}).ToList();
				return filterDataModel;
			}
		}

	}
}
