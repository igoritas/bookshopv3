﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.DataAccessLayer.Models;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter
{
	public static class CustomerMapper<T, U> where T : class where U : class
	{
		static public FilterResponseModel<T> FilterResponseDataModelToFilterResponseModel(FilterResponseDataModel<U> filterResponse)
		{
			var filterResponseModel = new FilterResponseModel<T>();
			filterResponseModel.AllCount = filterResponse.AllCount;
			return filterResponseModel;
		}
	}
}
