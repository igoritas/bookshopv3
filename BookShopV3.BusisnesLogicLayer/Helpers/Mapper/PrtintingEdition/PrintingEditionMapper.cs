﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using authorM = BookShopV3.BusisnesLogicLayer.Helpers.Mapper.AuthorMapper.CustomerMapper.AuthorMapper;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Mapper.PrtintingEdition
{
	public static partial class CustomerMapper
	{
		public class PrintingEditionMapper
		{
			public static  List<PrintingEditionWithAuthorModel> BookEntityToModelBook(List<PrintingEdition> books)
			{
				var bookModelList = books.Select(x => new PrintingEditionWithAuthorModel(x)
				{
					Authors = x.AuthorBooks.Select(x => new AuthorWithPrintingEditionModel(x.Author)).ToList()
				}).ToList();
				
				return bookModelList;
			}
			
			private static List<AuthorInPrintingEdition> PEWithAuthorModelToAuthorInPEntity(PrintingEditionWithAuthorModel authorBookModel)
			{
				var authorInPrintingEditions = authorBookModel.Authors.Select(x => new AuthorInPrintingEdition()
				{
					Author = authorM.AuthorModelToEntity(x),
					PrintingEdition = PrintingEditionModelToEntity(authorBookModel)
				}).ToList();
				return authorInPrintingEditions;
			}
			public static  PrintingEdition PrintingEditionModelToEntity(PrintingEditionWithAuthorModel printingEditionModel)
			{
				var printingEdition = new PrintingEdition();
				printingEdition.Id = (!string.IsNullOrWhiteSpace(printingEditionModel.Id)) ? new Guid(printingEditionModel.Id) : new Guid();
				printingEdition.Title = printingEditionModel.Name;
				printingEdition.Price = printingEditionModel.Price;
				printingEdition.Type = (PrintingEditionType)printingEditionModel.Type;
				printingEdition.Currency = printingEditionModel.Currency;
				printingEdition.PublicationDate = printingEditionModel.PublicationDate;
				printingEdition.Discription = printingEditionModel.Discription;
				return printingEdition;
			}
			public static  PrintingEditionWithAuthorModel PrintingEditionEntityToModelWithAuthor(PrintingEdition book)
			{
				var bookResult = new PrintingEditionWithAuthorModel(book)
				{
					Authors = book.AuthorBooks.Select(x => new AuthorWithPrintingEditionModel(x.Author)).ToList()
				};
				return bookResult;
			}
			public static  PrintingEdition BookModelToBookEntity(PrintingEditionWithAuthorModel book)
			{
				var tmp = PEWithAuthorModelToAuthorInPEntity(book);
				var bookResult = PrintingEditionModelToEntity(book);
				bookResult.AuthorBooks = tmp;
				return bookResult;
			}
			
		}
	}
}
