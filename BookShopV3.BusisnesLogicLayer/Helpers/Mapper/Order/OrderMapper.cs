﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using printingEditionM = BookShopV3.BusisnesLogicLayer.Helpers.Mapper.PrtintingEdition.CustomerMapper.PrintingEditionMapper;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Mapper.OrderMapper
{
	public static partial class CustomerMapper
	{
		public class OrderMapper
		{
			static public OrderModel OrderEntityToOrderModel(Order order)
			{
				return new OrderModel(order);
			}
			static private Order OrderModelToEntity(OrderModel orderModel,List<OrderItem> orderItems)
			{
				var order = new Order();
				order.Id = (!string.IsNullOrWhiteSpace(orderModel.Id)) ? new Guid(orderModel.Id) : new Guid();
				order.PayStatus = orderModel.IsPaid;
				order.Summ = orderModel.TotalPrice;
				order.OrderItems = orderItems;
				order.ApplicationUser = orderModel.User;
				order.Currency = orderModel.Currency;
				order.CreationDate = orderModel.Date;
				order.Payment = new Payment()
				{
					TransactionId = orderModel.TransactionId
				};
				return order;
			}
			static public Order OrderModelToOrderEntity(OrderModel orderModel)
			{
				
				var orderItems = orderModel.OrderItems.Select(item => new OrderItem
				{
					Id = (!string.IsNullOrWhiteSpace(item.Id)) ? new Guid(item.Id) : new Guid(),
					Count = item.Count,
					Amount = item.Price,
					Book = printingEditionM.PrintingEditionModelToEntity(item.Book)
				}).ToList();
				return OrderModelToEntity(orderModel, orderItems);
			}
		}
	}
}
