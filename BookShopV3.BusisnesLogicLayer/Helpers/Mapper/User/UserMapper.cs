﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Models;
using BookShopV3.DataAccessLayer.Entities;
using System.Linq;
using orderM = BookShopV3.BusisnesLogicLayer.Helpers.Mapper.OrderMapper.CustomerMapper.OrderMapper;

namespace BookShopV3.BusisnesLogicLayer.Helpers.Mapper.User
{
	public partial class CustomerMapper
	{
		public class UserMapper
		{

			static public UserModel UserEntityToUserModel(ApplicationUser user)
			{
				return new UserModel(user);
			}
			static public ApplicationUser UserModelToUserEntity(UserModel user)
			{
				var ordersEntity = user.Orders.Select(x => orderM.OrderModelToOrderEntity(x)).ToList();
				var userEntity = new ApplicationUser()
				{
					Id = user.Id,
					FirstName = user.FirstName,
					LastName = user.LastName,
					Email = user.Email,
					IsActive = user.IsActive,
					Orders = ordersEntity
				};
				return userEntity;
			}
			
			
			static public ApplicationUser RegisterModelToUserEntity(RegisterViewModel registerViewModel)
			{
				var user = new ApplicationUser();
				user.Email = registerViewModel.Email;
				user.UserName = registerViewModel.Login;
				user.FirstName = registerViewModel.FirstName;
				user.LastName = registerViewModel.LastName;
				return user;
			}
		}
	}
}
