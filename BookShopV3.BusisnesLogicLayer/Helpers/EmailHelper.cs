﻿using BookShopV3.BusisnesLogicLayer.Common;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BookShopV3.BusisnesLogicLayer.Services
{
	public class EmailHelper : IEmailService
	{
		private readonly MailAddress _fromAddress;
		private readonly SmtpClient _smtpClient;
		public EmailHelper(IOptions<EmailOption> options)
		{
			_fromAddress = new MailAddress(options.Value.FromAddress, options.Value.DisplayName);
			_smtpClient = new SmtpClient(options.Value.Host, options.Value.Port)
			{
				Credentials = new NetworkCredential(options.Value.UserName, options.Value.Password),
				EnableSsl = true
			};
		}

		public async Task SendEmailAsync(string toEmail, string text, string subject)
		{
			var toMailAddress = new MailAddress(toEmail);
			var mailMessage = new MailMessage(_fromAddress, toMailAddress);
			mailMessage.Subject = subject;
			mailMessage.Body = text;
			mailMessage.IsBodyHtml = true;
			await _smtpClient.SendMailAsync(mailMessage);
		}
	}
}
