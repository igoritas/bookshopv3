﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV3.BusisnesLogicLayer.Interfaces
{
	public interface IAuthorService
	{
		Task<AuthorWithPrintingEditionModel> CreateAsync(AuthorWithPrintingEditionModel authorModel);
		Task<AuthorWithPrintingEditionModel> GetByIdAsync(string id);
		Task<IEnumerable<AuthorWithPrintingEditionModel>> GetAllAsync();
		Task<int> UpdateAsync(AuthorWithPrintingEditionModel authorModel);
		Task<int> DeleteAsync(string id);
		Task<FilterResponseModel<AuthorWithPrintingEditionModel>> GetAllAsync(FilterRequestModel filterRequestModel);
	}
}
