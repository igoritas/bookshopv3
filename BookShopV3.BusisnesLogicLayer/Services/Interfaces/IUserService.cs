﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Models;
using System.Threading.Tasks;

namespace BookShopV3.BusisnesLogicLayer.Interfaces
{
	public interface IUserService
	{
		Task<UserModel> CreateAsync(RegisterViewModel user);
		Task<UserModel> GetByIdAsync(string id);
		Task<BaseModel> UpdateAsync(EditUserModel user, string userId);
		Task<BaseModel> DeleteAsync(string id);
		Task<FilterResponseModel<UserModel>> GetFilteringUserAsync(FilterRequestModel filter);
		Task<UserModel> LoginAsync(string login, string password);
		Task<string> GenerateEmailConfirmationTokenAsync(UserModel userModel);
		Task LogOutAsync();
		Task SignInAsync(UserModel userModel);
		Task<UserModel> FindByEmailAsync(string email);
		Task<BaseModel> ConfirmEmailAsync(string email, string code);
		Task<BaseModel> ForgotPasswordAsync(string email);

		Task SendEmailComfirmTokenAsync(string url, string email);
	}
}
