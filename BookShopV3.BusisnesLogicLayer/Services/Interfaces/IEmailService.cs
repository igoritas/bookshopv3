﻿using System.Threading.Tasks;

namespace BookShopV3.BusisnesLogicLayer.Interfaces
{
	public interface IEmailService
	{
		Task SendEmailAsync(string to, string text, string subject);
	}
}
