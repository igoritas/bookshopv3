﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Models.FilterModel;
using System.Threading.Tasks;

namespace BookShopV3.BusisnesLogicLayer.Interfaces
{
	public interface IPrintingEditionService
	{
		Task<FilterResponseModel<PrintingEditionWithAuthorModel>> GetAllAsync(FilterPrintingEditionModel filterRequestModel);
		Task<PrintingEditionWithAuthorModel> GetByIdAsync(string id);
		Task<int> UpdateAsync(PrintingEditionWithAuthorModel book);
		Task<int> DeleteAsync(string id);
		Task<PrintingEditionWithAuthorModel> AddAsync(PrintingEditionWithAuthorModel book);

	}
}
