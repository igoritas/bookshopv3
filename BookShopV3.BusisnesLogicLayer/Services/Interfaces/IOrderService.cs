﻿using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using System.Threading.Tasks;

namespace BookShopV3.BusisnesLogicLayer.Interfaces
{
	public interface IOrderService
	{
		Task<OrderModel> GetByIdAsync(string id);
		Task<OrderModel> CreateAsync(OrderModel order);
		Task<BaseModel> UpdateAsync(OrderModel order);
		Task<BaseModel> DeleteAsync(string id);
		Task<FilterResponseModel<OrderModel>> GetAllAsync(FilterRequestModel filterRequestModel);
	}
}
