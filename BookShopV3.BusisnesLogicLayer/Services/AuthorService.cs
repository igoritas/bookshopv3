﻿using BookShopV3.BusinessLogicLayer.Constant.Error;
using BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter;
using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using authorMapper = BookShopV3.BusisnesLogicLayer.Helpers.Mapper.AuthorMapper.CustomerMapper.AuthorMapper;
using filterMapper = BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter.CustomerMapper.FilterMapper;

namespace BookShopV3.BusisnesLogicLayer.Services
{
	public class AuthorService : IAuthorService
	{
		private readonly IAuthorRepository _authorRepository;
		public AuthorService(IAuthorRepository authorRepository)
		{
			_authorRepository = authorRepository;
		}

		public async Task<AuthorWithPrintingEditionModel> CreateAsync(AuthorWithPrintingEditionModel authorWithPrintingEdition)
		{
			var baseModel = new BaseModel();
			var authorEntity = authorMapper.AuthorWithPrintingEditionModelToEntity(authorWithPrintingEdition);
			var result = await _authorRepository.CreateAsync(authorEntity);
			if (result is null)
			{
				baseModel.Errors.Add(BusinessLogicLayerСonstant.ErrorСonstant.ErrorAuthorСonstant.Create);
			}
			var authorModel = authorMapper.AuthorEntityToAuthorModel(result);
			authorModel.Errors = baseModel.Errors;
			return authorModel;
		}

		public async Task<int> DeleteAsync(string id)
		{
			return await _authorRepository.DeleteAsync(id);

		}

		public async Task<IEnumerable<AuthorWithPrintingEditionModel>> GetAllAsync()
		{
			var authorsEntity = await _authorRepository.GetAllAsync();
			var authorModel = new List<AuthorWithPrintingEditionModel>();
			foreach (var item in authorsEntity)
			{
				authorModel.Add(authorMapper.AuthorEntityToAuthorModel(item));
			}
			return authorModel;
		}

		public async Task<AuthorWithPrintingEditionModel> GetByIdAsync(string id)
		{
			var author = await _authorRepository.GetByIdAsync(id);
			var result = authorMapper.AuthorEntityToAuthorModel(author);
			return result;
		}

		public async Task<FilterResponseModel<AuthorWithPrintingEditionModel>> GetAllAsync(FilterRequestModel filterRequestModel)
		{
			var filterData = filterMapper.FilterRequesModelToFilterDataModel(filterRequestModel);
			var filterResponseDataModel = await _authorRepository.GetFilteredAsync(filterData);
			var responseData = CustomerMapper<AuthorWithPrintingEditionModel, Author>.FilterResponseDataModelToFilterResponseModel(filterResponseDataModel);
			foreach (var item in filterResponseDataModel.Items)
			{
				responseData.Items.Add(authorMapper.AuthorEntityToAuthorModel(item));
			}
			return responseData;
		}

		public async Task<int> UpdateAsync(AuthorWithPrintingEditionModel authorWithPrintingEdition)
		{
			var authorEntity = authorMapper.AuthorWithPrintingEditionModelToEntity(authorWithPrintingEdition);
			return await _authorRepository.UpdateAsync(authorEntity);
		}
	}
}
