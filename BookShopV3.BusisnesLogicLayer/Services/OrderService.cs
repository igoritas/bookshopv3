﻿using BookShopV3.BusinessLogicLayer.Constant.Error;
using BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter;
using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using static BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter.CustomerMapper;
using static BookShopV3.BusisnesLogicLayer.Helpers.Mapper.OrderMapper.CustomerMapper;

namespace BookShopV3.BusisnesLogicLayer.Services
{
	public class OrderService : IOrderService
	{
		readonly private IOrderRepository _orderRepository;
		readonly private IOrderItemRepository _orderItemRepository;
		public OrderService(IOrderRepository orderRepository, IOrderItemRepository orderItemRepository)
		{
			_orderRepository = orderRepository;
			_orderItemRepository = orderItemRepository;

		}
		public async Task<OrderModel> CreateAsync(OrderModel order)
		{
			var baseModel = new OrderModel();
			var resultOrder = OrderMapper.OrderModelToOrderEntity(order);
			resultOrder.CreationDate = DateTime.UtcNow;
			var orderResult = await _orderRepository.CreateAsync(resultOrder);
			if (orderResult is null)
			{
				baseModel.Errors.Add(BusinessLogicLayerСonstant.ErrorСonstant.ErrorOrderСonstant.Create);
				return baseModel;
			}
			foreach (var item in resultOrder.OrderItems.ToList())
			{
				var result = await _orderItemRepository.CreateAsync(item);
			}
			var response = OrderMapper.OrderEntityToOrderModel(orderResult);
			return response;
		}
		public async Task<BaseModel> DeleteAsync(string id)
		{
			var baseModel = new BaseModel();
			var result = await _orderRepository.DeleteAsync(id);
			if (result == 0)
			{
				baseModel.Errors.Add(BusinessLogicLayerСonstant.ErrorСonstant.ErrorOrderСonstant.Delete);
			}
			return baseModel;
		}
		public async Task<OrderModel> GetByIdAsync(string id)
		{
			var baseModel = new OrderModel();
			var orderEnity = OrderMapper.OrderEntityToOrderModel(await _orderRepository.GetByIdAsync(id));
			if (orderEnity is null)
			{
				baseModel.Errors.Add(BusinessLogicLayerСonstant.ErrorСonstant.ErrorOrderСonstant.Filtring);
				return baseModel;
			}
			return orderEnity;
		}

		public async Task<FilterResponseModel<OrderModel>> GetAllAsync(FilterRequestModel filterRequestModel)
		{
			var filterData = FilterMapper.FilterRequesModelToFilterDataModel(filterRequestModel);
			var filterResponseDataModel = await _orderRepository.GetFilteredAsync(filterData);
			var responseData = CustomerMapper<OrderModel, Order>.FilterResponseDataModelToFilterResponseModel(filterResponseDataModel);
			foreach (var item in filterResponseDataModel.Items)
			{
				responseData.Items.Add(OrderMapper.OrderEntityToOrderModel(item));
			}
			return responseData;
		}
		public async Task<BaseModel> UpdateAsync(OrderModel order)
		{
			var baseModel = new BaseModel();
			var orderEntity = OrderMapper.OrderModelToOrderEntity(order);
			var result = await _orderRepository.UpdateAsync(orderEntity);
			if (result == 0)
			{
				baseModel.Errors.Add(BusinessLogicLayerСonstant.ErrorСonstant.ErrorOrderСonstant.Update);
			}
			return baseModel;
		}
	}
}
