﻿using BookShopV3.BusinessLogicLayer.Helpers.Interfaces;
using BookShopV3.BusisnesLogicLayer.Common;
using BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter;
using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using BookShopV3.BusisnesLogicLayer.Models;
using BookShopV3.BusisnesLogicLayer.Models.Enums;
using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter.CustomerMapper;
using static BookShopV3.BusisnesLogicLayer.Helpers.Mapper.User.CustomerMapper;
using errorConstant = BookShopV3.BusinessLogicLayer.Constant.Error.BusinessLogicLayerСonstant.ErrorСonstant;
using infoConstant = BookShopV3.BusinessLogicLayer.Constant.Information.BusinessLogicLayerСonstant.InformationСonstant;

namespace BookShopV3.BusisnesLogicLayer.Services
{
	public class UserService : IUserService
	{
		private readonly IUserRepository _userRepository;
		private readonly IEmailService _emailService;
		private readonly IOptions<GeneratePasswordOption> _generatePasswordOption;
		private readonly IHttpHelper _httpHelper;

		public UserService(IUserRepository userRepository, IEmailService emailService, IOptions<GeneratePasswordOption> generatePasswordOption, IHttpHelper httpHelper)
		{
			_userRepository = userRepository;
			_emailService = emailService;
			_generatePasswordOption = generatePasswordOption;
			_httpHelper = httpHelper;
		}
		public async Task<UserModel> CreateAsync(RegisterViewModel model)
		{
			var userResponse = new UserModel();
			var user = UserMapper.RegisterModelToUserEntity(model);
			var result = await _userRepository.CreateUserWithPassword(user, model.Password);
			userResponse.Errors = result.Errors.Select(x => x.Description).ToList();
			var userEntity = await _userRepository.FindByEmailAsync(user.Email);
			if (userEntity is null)
			{
				userResponse.Errors.Add(errorConstant.ErrorUserСonstant.Filtring);
				return userResponse;
			}
			var resultAddRole = await _userRepository.AddToRoleAsync(userEntity, RoleType.User.ToString());
			userResponse.Errors.AddRange(resultAddRole.Errors.Select(x => x.Description).ToList());
			userResponse = UserMapper.UserEntityToUserModel(userEntity);
			return userResponse;
		}

		public async Task<BaseModel> DeleteAsync(string id)
		{
			var baseModel = new BaseModel();
			var user = await _userRepository.FindByIdAsync(id);
			user.IsActive = false;
			if (!(await _userRepository.UpdateAsync(user)).Succeeded)
			{
				baseModel.Errors.Add(errorConstant.ErrorUserСonstant.Delete);
			}
			return baseModel;
		}

		public async Task<UserModel> LoginAsync(string login, string password)
		{
			var userModel = new UserModel();
			var result =
			await _userRepository.PasswordSignInAsync(login, password);
			if (!result.Succeeded)
			{
				userModel.Errors.Add(result.ToString());
			}
			var user = await _userRepository.FindByLoginAsync(login);
			if (user is null)
			{
				userModel.Errors.Add(errorConstant.ErrorUserСonstant.Filtring);
				return userModel;
			}
			if (!user.IsActive)
			{
				userModel.Errors.Add(errorConstant.ErrorUserСonstant.IsNotActive);
				return userModel;
			}
			userModel = UserMapper.UserEntityToUserModel(user);
			if (!user.EmailConfirmed)
			{
				userModel.Errors.Add(errorConstant.ErrorLoginСonstant.EmailIsNotComfirmed);
				return userModel;
			}
			userModel.Roles = await _userRepository.GetRolesAsync(user);
			return userModel;
		}

		public async Task<UserModel> GetByIdAsync(string id)
		{
			var user = await _userRepository.GetAsync(id);
			if (user is null)
			{
				var errors = new UserModel();
				errors.Errors.Add(errorConstant.ErrorPrintingEditionСonstant.Filtring);
				return errors;
			}
			return UserMapper.UserEntityToUserModel(user);
		}

		public async Task<FilterResponseModel<UserModel>> GetFilteringUserAsync(FilterRequestModel filterRequestModel)
		{
		
			var filterData = FilterMapper.FilterRequesModelToFilterDataModel(filterRequestModel);
			var filterResponse = await _userRepository.GetFilteredUsersAsync(filterData);
			var responseData = CustomerMapper<UserModel, ApplicationUser>.FilterResponseDataModelToFilterResponseModel(filterResponse);
			foreach (var item in filterResponse.Items)
			{
				responseData.Items.Add(UserMapper.UserEntityToUserModel(item));
			}
			return responseData;
		}
		public async Task<BaseModel> UpdateAsync(EditUserModel user, string userId)
		{
			var baseModel = new BaseModel();
			var userEntity = await _userRepository.FindByEmailAsync(user.Email);
			if (userEntity is null)
			{
				userEntity = await _userRepository.FindByIdAsync(userId);
			}
			if (userEntity is null)
			{
				baseModel.Errors.Add(errorConstant.ErrorUserСonstant.NoSuchUser);
				return baseModel;
			}
			userEntity.FirstName = user.FirstName;
			userEntity.LastName = user.LastName;
			if (!string.IsNullOrWhiteSpace(user.OldPassword) && !string.IsNullOrWhiteSpace(user.NewPassword))
			{
				var resultChangePassword = await _userRepository.ChangePasswordAsync(userEntity, user.OldPassword, user.NewPassword);
				baseModel.Errors.AddRange(resultChangePassword.Errors.Select(x => x.Description));
			}
			if (!user.Email.Equals(user.Email) && !string.IsNullOrWhiteSpace(user.Email))
			{
				var tokenEmailChenge = await _userRepository.GenerateChangeEmailTokenAsync(userEntity);
				var resultChangeEmail = await _userRepository.ChangeEmailAsync(userEntity, tokenEmailChenge);
				baseModel.Errors.AddRange(resultChangeEmail.Errors.Select(x => x.Description));
			}
			var resultUpdateUser = await _userRepository.UpdateAsync(userEntity);
			baseModel.Errors.AddRange(resultUpdateUser.Errors.Select(x => x.Description));
			var result = UserMapper.UserEntityToUserModel(userEntity);

			result.Errors = baseModel.Errors;
			return result;
		}

		public async Task LogOutAsync()
		{
			await _userRepository.SignOutAsync();
		}

		public async Task<string> GenerateEmailConfirmationTokenAsync(UserModel userModel)
		{
			var userApp = UserMapper.UserModelToUserEntity(userModel);
			return await _userRepository.GenerateEmailConfirmationTokenAsync(userApp);
		}

		public async Task SignInAsync(UserModel userModel)
		{
			var userApp = UserMapper.UserModelToUserEntity(userModel);
			await _userRepository.SignInAsync(userApp);
		}

		public async Task<UserModel> FindByEmailAsync(string email)
		{
			var baseModel = new BaseModel();
			var userEntity = await _userRepository.FindByEmailAsync(email);
			if (userEntity is null)
			{
				baseModel.Errors.Add(errorConstant.ErrorUserСonstant.NoSuchUser);
				return baseModel as UserModel;
			}
			var result = UserMapper.UserEntityToUserModel(userEntity);
			return result;
		}

		public async Task<BaseModel> ConfirmEmailAsync(string email, string code)
		{
			var errors = new BaseModel();
			var identityResult = new IdentityResult();
			var user = await _userRepository.FindByEmailAsync(email);
			if (user is null)
			{
				errors.Errors.Add(errorConstant.ErrorUserСonstant.NoSuchUser);
				return errors;
			}
			identityResult = await _userRepository.ConfirmEmailAsync(user, code);
			errors.Errors = identityResult.Errors.Select(x => x.Description).ToList();
			return errors;
		}

		public async Task<BaseModel> ForgotPasswordAsync(string email)
		{
			var errors = new BaseModel();
			var passwordChangeResult = new IdentityResult();
			var user = await _userRepository.FindByEmailAsync(email);
			if (user is null)
			{
				errors.Errors.Add(errorConstant.ErrorUserСonstant.NoSuchUser);
				return errors;
			}
			var newPassword = new StringBuilder("");
			var passwordList = JObject.Parse(await _httpHelper.GetHttpContent(_generatePasswordOption.Value.Url))["response"]["items"];
			newPassword.Append(passwordList.First.ToString());
			newPassword.Append("*");
			await _emailService.SendEmailAsync(email, $@"<h1> Your new password {newPassword}<h1>", "New Password");
			var resetToken = await _userRepository.GeneratePasswordResetTokenAsync(user);
			passwordChangeResult = await _userRepository.ResetPasswordAsync(user, resetToken, newPassword.ToString());
			errors.Errors = passwordChangeResult.Errors.Select(x => x.Description).ToList();
			return errors;
		}

		public async Task SendEmailComfirmTokenAsync(string url, string email)
		{
			await _emailService.SendEmailAsync(email,
			$"{infoConstant.InfoEmailConstant.ConfimEmailBody} <a href='{url}'>link</a>",
			infoConstant.InfoEmailConstant.ConfimEmailHeader);
		}
	}
}
