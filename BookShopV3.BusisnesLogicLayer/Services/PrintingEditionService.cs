﻿using BookShopV3.BusinessLogicLayer.Constant.Error;
using BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter;
using BookShopV3.BusisnesLogicLayer.Helpers.Models;
using BookShopV3.BusisnesLogicLayer.Helpers.Models.PageModel;
using BookShopV3.BusisnesLogicLayer.Interfaces;
using BookShopV3.BusisnesLogicLayer.Models.FilterModel;
using BookShopV3.DataAccessLayer.Entities;
using BookShopV3.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fMapper = BookShopV3.BusisnesLogicLayer.Helpers.Mapper.Filter.CustomerMapper.FilterMapper;
using peMapper = BookShopV3.BusisnesLogicLayer.Helpers.Mapper.PrtintingEdition.CustomerMapper.PrintingEditionMapper;

namespace BookShopV3.BusisnesLogicLayer.Services
{
	public class PrintingEditionService : IPrintingEditionService
	{
		readonly private IPrintingEditionRepository _printingEditionRepository;
		readonly private IAuthorInPrintingEditionRepository _authorInPrintingEditionRepository;

		public PrintingEditionService(IPrintingEditionRepository repository, IAuthorInPrintingEditionRepository athorBookRepository) =>
			(_printingEditionRepository, _authorInPrintingEditionRepository) = (repository, athorBookRepository);

		public async Task<PrintingEditionWithAuthorModel> AddAsync(PrintingEditionWithAuthorModel bookModel)
		{
			var baseModel = new PrintingEditionWithAuthorModel();
			var book = peMapper.BookModelToBookEntity(bookModel);
			var result = await _printingEditionRepository.CreateAsync(book);
			if (result is null)
			{
				baseModel.Errors.Add(BusinessLogicLayerСonstant.ErrorСonstant.ErrorPrintingEditionСonstant.Create);
				return baseModel;
			}
			var authorInPrintingEdition = new List<AuthorInPrintingEdition>();
			foreach (var item in book.AuthorBooks.ToList())
			{
				var tmpAthorBook = new AuthorInPrintingEdition()
				{
					Author = item.Author,
					PrintingEdition = item.PrintingEdition
				};
				authorInPrintingEdition.Add(await _authorInPrintingEditionRepository.CreateAsync(tmpAthorBook));
			}
			result.AuthorBooks = authorInPrintingEdition;
			await _printingEditionRepository.UpdateAsync(result);
			return peMapper.PrintingEditionEntityToModelWithAuthor(book);
		}

		public async Task<int> DeleteAsync(string id)
		{
			return await _printingEditionRepository.DeleteAsync(id);
		}

		public async Task<PrintingEditionWithAuthorModel> GetByIdAsync(string id)
		{
			var printingEdition = await _printingEditionRepository.GetByIdAsync(id);
			if (printingEdition is null)
			{
				var errors = new PrintingEditionWithAuthorModel();
				errors.Errors.Add(BusinessLogicLayerСonstant.ErrorСonstant.ErrorPrintingEditionСonstant.Filtring);
				return errors;
			}
			var bookResult = peMapper.PrintingEditionEntityToModelWithAuthor(printingEdition);
			return bookResult;
		}

		public async Task<FilterResponseModel<PrintingEditionWithAuthorModel>> GetAllAsync(FilterPrintingEditionModel filterRequestModel)
		{
			var filterData = fMapper.FilterRequesModelToFilterDataModel(filterRequestModel);
			var filterResponseDataModel = await _printingEditionRepository.GetFilteredBookAsync(filterData, filterRequestModel.MinPrice, filterRequestModel.MaxPrice);
			var responseData = CustomerMapper<PrintingEditionWithAuthorModel, PrintingEdition>.FilterResponseDataModelToFilterResponseModel(filterResponseDataModel);
			responseData.Items = peMapper.BookEntityToModelBook(filterResponseDataModel.Items.ToList());
			return responseData;
		}

		public async Task<int> UpdateAsync(PrintingEditionWithAuthorModel bookModel)
		{
		
			var printingEdition = peMapper.BookModelToBookEntity(bookModel);
				var authorInPrintingEditions = _authorInPrintingEditionRepository.Find(ab => ab.PrintingEdition.Id == printingEdition.Id);
			if (authorInPrintingEditions is null)
			{
				return 0;
			}
			foreach (var item in authorInPrintingEditions)
			{
				var result = await _authorInPrintingEditionRepository.DeleteAsync(item.Id.ToString());
			}
			var authorInPrintingEdition = new List<AuthorInPrintingEdition>();
			foreach (var item in printingEdition.AuthorBooks)
			{
				var tmpAthorBook = new AuthorInPrintingEdition()
				{
					Author = item.Author,
					PrintingEdition = item.PrintingEdition
				};
				authorInPrintingEdition.Add(await _authorInPrintingEditionRepository.CreateAsync(tmpAthorBook));
			}
			printingEdition.AuthorBooks = authorInPrintingEdition;
			return await _printingEditionRepository.UpdateAsync(printingEdition);
		}
	}
}
